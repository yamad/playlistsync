//
//  DirectoryEventTests.m
//  DirectoryEventTests
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "JYHRandomAudioFolderGenerator.h"
#import "YPSDirectorySnapshot.h"

@interface YPSDirectorySnapshotTests : XCTestCase
@property NSURL *testDirURL;
@end

@implementation YPSDirectorySnapshotTests

- (void)setUp {
    [super setUp];
    // identify random test directory
    _testDirURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"psTest"] isDirectory:YES];
}

- (void)tearDown {
    [super tearDown];

    // delete random test directory
    [[NSFileManager defaultManager] removeItemAtURL:_testDirURL error:nil];
}


- (void) testThatSnapshotSavesAllFiles {
    NSUInteger expectedFileCount = 2;

    // given a directory with two files
    [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:_testDirURL
                                                     count:expectedFileCount
                                               maxDuration:1];

    // then, a snapshot contains the right number of files
    YPSDirectorySnapshot *snap = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];
    XCTAssertEqual(expectedFileCount, [snap.files count]);

    // and the right names
    NSSet *allFiles = [NSSet setWithArray:[[NSFileManager defaultManager]
                                           contentsOfDirectoryAtURL:_testDirURL
                                           includingPropertiesForKeys:nil
                                           options:nil error:nil]];

    XCTAssertEqualObjects(allFiles, snap.files);
}

- (void) testThatSnapshotCapturesSubdirectories {
    // given a subdirectory with 1 file, but no local files
    NSURL *subdirURL = [NSURL fileURLWithPath:@"subdir" isDirectory:YES relativeToURL:_testDirURL];
    [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:subdirURL count:1 maxDuration:1];

    // then, a snapshot
    YPSDirectorySnapshot *snap = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // contains no direct files, but has them stored in all files
    XCTAssertEqual(0, [snap.files count]);
    XCTAssertEqual(1, [snap.allFiles count]);

    // contains a snapshot of the subdirectory
    XCTAssertEqual(1, [snap.subdirs count]);
    // TODO: deal with URL equality.
//    YPSDirectorySnapshot *subSnap = snap.subdirs[[subdirURL URLByStandardizingPath]];
//    XCTAssertNotNil(subSnap);
//    XCTAssertEqual(1, [subSnap.files count]);
}

- (void) testThatSnapshotDoesNotChange {
    // given a snapshot of a directory with 2 files and subdirectory with 1 file
    [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:_testDirURL
                                                     count:2
                                               maxDuration:1];
    NSURL *subdirURL = [NSURL fileURLWithPath:@"subdir" isDirectory:YES relativeToURL:_testDirURL];
    [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:subdirURL count:1 maxDuration:1];

    // save pre-change statistics
    YPSDirectorySnapshot *snap = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];
    NSInteger fileCount    = [snap.files count];
    NSInteger allFileCount = [snap.allFiles count];
    NSInteger subdirCount  = [snap.subdirs count];
    NSDate *timestamp      = [snap.timestamp copy];

    // change directory
    [[NSFileManager defaultManager] removeItemAtURL:snap.files.allObjects[0] error:nil];
    [[NSFileManager defaultManager] removeItemAtURL:subdirURL error:nil];

    // then, snapshot does not change value after a change in the directory
    XCTAssertEqual(fileCount, [snap.files count]);
    XCTAssertEqual(allFileCount, [snap.allFiles count]);
    XCTAssertEqual(subdirCount, [snap.subdirs count]);
    XCTAssertEqualObjects(timestamp, snap.timestamp);
}

- (void) DISABLED_testThatInvalidURLReturnsNil {
    // TODO!
    XCTAssertNil([[YPSDirectorySnapshot alloc] initWithURL:[NSURL fileURLWithPath:@"/not/a/real/path/123451"]]);
}

@end
