//
//  YPSDirectoryComparatorTests.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "YPSDirectorySnapshot.h"
#import "YPSDirectoryComparator.h"
#import "JYHRandomAudioFolderGenerator.h"

@interface YPSDirectoryComparatorTests : XCTestCase
@property NSURL *testDirURL;
@end

@implementation YPSDirectoryComparatorTests

- (void)setUp {
    [super setUp];
    // identify random test directory
    _testDirURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"psTest"] isDirectory:YES];
}

- (void)tearDown {
    [super tearDown];

    // delete random test directory
    [[NSFileManager defaultManager] removeItemAtURL:_testDirURL error:nil];
}

- (void) testThatItDetectsNoChangesIfNone {
    NSUInteger expectedFileCount = 2;

    // given a directory with two files
    [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:_testDirURL
                                                     count:expectedFileCount
                                               maxDuration:1];

    // and no changes between two snapshots
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];
    sleep(2);
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // then comparator reports no changes
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1
                                                                          andOther:snap2];
    XCTAssertFalse(cmp.hasChanges);
    XCTAssertFalse(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqual(0, [cmp.filesInLeftOnly count]);
    XCTAssertEqual(0, [cmp.filesInRightOnly count]);
    XCTAssertEqual(0, [cmp.dirsInLeftOnly count]);
    XCTAssertEqual(0, [cmp.dirsInRightOnly count]);
    XCTAssertEqual(0, [cmp.filesInLeftOnlyDirs count]);
    XCTAssertEqual(0, [cmp.filesInRightOnlyDirs count]);
    XCTAssertEqual(0, [cmp.filesChanged count]);
}

- (void) testThatItDetectsAddition {
    NSArray *fileList = @[@"a.txt"];
    [JYHRandomAudioFolderGenerator generateFromList:fileList relativeToURL:_testDirURL];

    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given an addition to a folder
    [JYHRandomAudioFolderGenerator generateFromList:@[@"b.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // then a comparator detects the change
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertTrue(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"b.txt", [cmp.filesInRightOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDeletion {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given removal of one file
    [[NSFileManager defaultManager] removeItemAtURL:snap1.files.allObjects[0] error:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // then the comparator detects the change
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertTrue(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"a.txt", [cmp.filesInLeftOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsModification {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given change to one file
    [[NSFileManager defaultManager] createFileAtPath:[snap1.files.allObjects[0] path]
                                            contents:[NSData dataWithBytes:(char[]){ 0x00 } length:1]
                                          attributes:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertTrue(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"a.txt", [cmp.filesChanged.allObjects[0] lastPathComponent]);
}


- (void) testThatItDetectsDeepAddition {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt", @"dir/a.txt"]
                                      relativeToURL:_testDirURL];

    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given an addition to a folder
    [JYHRandomAudioFolderGenerator generateFromList:@[@"dir/b.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // then a comparator detects the change
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertFalse(cmp.hasShallowChanges);
    XCTAssertTrue(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"b.txt", [cmp.allFilesInRightOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDeepDeletion {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt", @"dir/b.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given removal of one file
    [[NSFileManager defaultManager] removeItemAtURL:snap1.allFiles.allObjects[1] error:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // then the comparator detects the change
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertFalse(cmp.hasShallowChanges);
    XCTAssertTrue(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"b.txt", [cmp.allFilesInLeftOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDeepModification {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"dir/a.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given change to one file
    [[NSFileManager defaultManager] createFileAtPath:[snap1.allFiles.allObjects[0] path]
                                            contents:[NSData dataWithBytes:(char[]){ 0x00 } length:1]
                                          attributes:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertFalse(cmp.hasShallowChanges);
    XCTAssertTrue(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"a.txt", [cmp.allFilesChanged.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDirAddition {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given addition of a directory
    [[NSFileManager defaultManager] createDirectoryAtURL:[NSURL fileURLWithPath:@"dir"
                                                                    isDirectory:YES
                                                                  relativeToURL:_testDirURL]
                             withIntermediateDirectories:YES
                                              attributes:nil error:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertTrue(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"dir", [cmp.dirsInRightOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDirDeletion {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt", @"dir/b.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given removal of a directory
    [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:@"dir"
                                                                    isDirectory:YES
                                                                  relativeToURL:_testDirURL]
                                              error:nil];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertTrue(cmp.hasShallowChanges);
    XCTAssertFalse(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"dir", [cmp.dirsInLeftOnly.allObjects[0] lastPathComponent]);
}

- (void) testThatItDetectsDirModification {
    [JYHRandomAudioFolderGenerator generateFromList:@[@"a.txt", @"dir/b.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap1 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    // given change in a subdirectory
    [JYHRandomAudioFolderGenerator generateFromList:@[@"dir/c.txt"] relativeToURL:_testDirURL];
    YPSDirectorySnapshot *snap2 = [[YPSDirectorySnapshot alloc] initWithURL:_testDirURL];

    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc] initWithSnapshot:snap1 andOther:snap2];

    XCTAssertTrue(cmp.hasChanges);
    XCTAssertFalse(cmp.hasShallowChanges);
    XCTAssertTrue(cmp.hasDeepChanges);

    XCTAssertEqualObjects(@"dir", [cmp.dirsChanged.allObjects[0] lastPathComponent]);
}

@end
