//
//  YPSFileComparatorTests.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/9/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "YPSFileComparator.h"

@interface YPSFileComparatorTests : XCTestCase

@end

@implementation YPSFileComparatorTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testThatFileStubComparatorMatches {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/temp1.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x03 } length:2] attributes:0];
    BOOL res = [YPSFilenameStubMatcher matchAtURL:[NSURL fileURLWithPath:f1] andURL:[NSURL fileURLWithPath:f2] error:nil];
    XCTAssertTrue(res);
}

- (void)testThatFileStubComparatorRejectsNonMatches {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp1.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/tmp2.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x03 } length:2] attributes:0];
    BOOL res = [YPSFilenameStubMatcher matchAtURL:[NSURL fileURLWithPath:f1] andURL:[NSURL fileURLWithPath:f2] error:nil];
    XCTAssertFalse(res);
}

- (void)testThatFileSizeComparatorMatches {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/temp1.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x03 } length:2] attributes:0];
    BOOL res = [YPSFileSizeMatcher matchAtURL:[NSURL fileURLWithPath:f1] andURL:[NSURL fileURLWithPath:f2] error:nil];
    XCTAssertTrue(res);
}

- (void)testThatFileSizeComparatorRejectsNonMatches {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp1.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/temp2.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02, 0x03 } length:3] attributes:0];
    BOOL res = [YPSFileSizeMatcher matchAtURL:[NSURL fileURLWithPath:f1] andURL:[NSURL fileURLWithPath:f2] error:nil];
    XCTAssertFalse(res);
}

- (void)testThatFileContentsComparatorIsFalseForUnequalFiles {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp1.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/temp2.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x03 } length:2] attributes:0];
    BOOL res = [YPSFileContentsMatcher matchAtURL:[NSURL fileURLWithPath:f1]
                                           andURL:[NSURL fileURLWithPath:f2]
                                            error:nil];
    XCTAssertFalse(res);
}

- (void)testThatFileContentsComparatorIsTrueForEqualFiles {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *f1 = [@"~/temp1.txt" stringByStandardizingPath];
    NSString *f2 = [@"~/temp2.txt" stringByStandardizingPath];
    [fm createFileAtPath:f1 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];
    [fm createFileAtPath:f2 contents:[NSData dataWithBytes:(char []){ 0x01, 0x02 } length:2] attributes:0];

    BOOL res = [YPSFileContentsMatcher matchAtURL:[NSURL fileURLWithPath:f1]
                                           andURL:[NSURL fileURLWithPath:f2]
                                            error:nil];

    XCTAssertTrue(res);
}

- (void)testThatDeletingPathExtensionIsntStupid {
    NSURL *u1 = [NSURL fileURLWithPath:@"/Users/jyh/music_test/Music/Unknown Artist/Unknown Album/Malala Mix 20.wav" ];
    NSURL *u2 = [NSURL fileURLWithPath:@"/Users/jyh/src/playlistsync/PSTestDir/Malalala Mix #JL/Malala Mix 20 .wav"];
    BOOL res = [YPSiTunesFileMatcher matchAtURL:u1 andURL:u2 error:nil];
    XCTAssertTrue(res);
}

@end
