//
//  YPSDirectorySnapshot.h
//  PlaylistSync
//
//  defines a tree that mirrors the directory hierarchy at `path` but taking a snapshot of file/subdirectory attributes at a time `timestamp`
//  used for comparing and discovering changes to the filesystem under `path`
//
//  Created by Jason Yamada-Hanff on 5/18/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@interface YPSDirectorySnapshot : NSObject

@property (readonly) NSSet<NSURL *> *files;
@property (readonly) NSURL *path;
@property (readonly) NSDate *timestamp;
@property (readonly) NSDictionary<NSURL *, id> *generationIDs;
@property (readonly) NSMutableDictionary<NSURL *, YPSDirectorySnapshot *> *subdirs;

@property (readonly) NSSet<NSURL *> *allFiles;

-(instancetype) initWithURL:(NSURL*)dirPath;
-(instancetype) initWithURL:(NSURL*)dirPath
                shallowOnly:(BOOL)shallowCheck NS_DESIGNATED_INITIALIZER;

/*! @abstract Use initWithURL instead */
-(instancetype) init NS_UNAVAILABLE;

-(NSDate *) makeSnapshotWithError:(NSError **)error;
-(NSDate *) makeSnapshotWithError:(NSError **)error
                      shallowOnly:(BOOL)shallowCheck;

@end
