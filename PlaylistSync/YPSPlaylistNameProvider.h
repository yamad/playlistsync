//
//  YPSPlaylistNameProvider.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/28/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@interface YPSPlaylistNameProvider : NSObject
@property (nonatomic, copy, readonly) NSString *defaultName;
@property (nonatomic, copy, readonly) NSDictionary<NSString*, NSString*> *playlists;
- (NSString *) getNameForTag:(NSString *) tag;
- (void) addDiscoveredTag:(NSString *)tag;
- (void) addName:(NSString *)name ForTag:(NSString *)tag;
- (void) removeForTag:(NSString *)tag;
- (void) stop; // cleanup
@end

@interface YPSExcelNameProvider : YPSPlaylistNameProvider
- (instancetype) initWithRootURL:(NSURL *) rootURL;
@end

@interface YPSInternalNameProvider : YPSPlaylistNameProvider
- (void) loadFromPlist;
- (void) saveToPlist;
@end