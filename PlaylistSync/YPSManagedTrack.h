//
//  YPSManagedTrack.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/24/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

// An immutable "structure" for holding linking information for tracked files and the corresponding iTunes library entry
@interface YPSManagedTrack : NSObject
@property (copy, readonly) NSURL    *fileURL;
@property (copy, readonly) NSDate   *trackDate;
@property (copy, readonly) NSNumber *fileSize;
@property (copy, readonly) NSURL    *itunesURL;
@property (copy, readonly) NSNumber *itunesDatabaseID;

- (instancetype) initWithURL:(NSURL*)url
                 andFileSize:(NSNumber*)fsize
                     andDbID:(NSNumber*)dbID
                andITunesURL:(NSURL*)itunesURL
                     andDate:(NSDate*)addDate NS_DESIGNATED_INITIALIZER;
- (instancetype) initWithURL:(NSURL*)url
                     andDbID:(NSNumber*)dbID
                andITunesURL:(NSURL*)itunesURL;

/*! @abstract Use initWithURL:... methods instead */
-(instancetype) init NS_UNAVAILABLE;
@end
