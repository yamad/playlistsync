//
//  YPSManagedTrackStore.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/9/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSManagedTrackStore.h"

#import "YPSManagedTrack.h"
#import "YPSFileComparator.h"

#import "logging.h"

@interface YPSManagedTrackStore ()
// private properties
@property NSMutableDictionary<NSURL *, YPSManagedTrack *> *tracksByURL;
@end

@implementation YPSManagedTrackStore

- (instancetype) init {
    self = [super init];
    if (!self) return nil;

    _tracksByURL = [NSMutableDictionary dictionary];
    return self;
}

- (BOOL) hasTrackAtURL:(NSURL *)fileURL {
    return ([_tracksByURL objectForKey:fileURL] != nil);
}

- (YPSManagedTrack *) getTrackAtFileURL:(NSURL *)fileURL {
    return [_tracksByURL objectForKey:fileURL];
}

- (BOOL) addTrackWithURL:(NSURL*)fileURL
                 andDbID:(NSNumber*)dbID
            andITunesURL:(NSURL*)itURL
              checkFileMatch:(BOOL)checkFileMatch {
    if (checkFileMatch) {
        if (![YPSiTunesFileMatcher matchAtURL:fileURL andURL:itURL error:nil]) {
            DDLogDebug(@"Files don't match:  file %@ and iTunes file %@ for track %@",
                       [fileURL path], [itURL path], dbID);
            return NO;
        } else {
            DDLogDebug(@"Files match deeply: file %@ and iTunes file %@ for track %@",
                       [fileURL path], [itURL path], dbID);
        }
    }

    YPSManagedTrack *newMT = [[YPSManagedTrack alloc] initWithURL:fileURL
                                                          andDbID:dbID
                                                     andITunesURL:itURL];
    if (newMT) {
        _tracksByURL[fileURL] = newMT;
        return YES;
    }
    return NO;
}

- (BOOL) addTrackWithURL:(NSURL*)fileURL
                 andDbID:(NSNumber*)dbID
            andITunesURL:(NSURL*)itURL {
    return [self addTrackWithURL:fileURL andDbID:dbID andITunesURL:itURL checkFileMatch:NO];
}

- (void) removeTrackAtURL:(NSURL *)fileURL {
    [_tracksByURL removeObjectForKey:fileURL];
}

- (NSUInteger) count {
    return _tracksByURL.count;
}

@end
