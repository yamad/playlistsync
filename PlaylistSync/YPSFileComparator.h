//
//  YPSFileComparator.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/8/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@protocol YPSFileComparator
+ (BOOL) matchAtURL:(NSURL *)url1 andURL:(NSURL *)url2 error:(NSError **)error;
@end

/** compose file comparators for series of file match checks 
 
 appropriate for matching up a general file with a iTunes media file
 */
@interface YPSiTunesFileMatcher : NSObject <YPSFileComparator>
@end

/** test whether file name stubs ("stub 1.wav", "stub.wav") */
@interface YPSFilenameStubMatcher : NSObject <YPSFileComparator>
@end

/** test whether file sizes match */
@interface YPSFileSizeMatcher : NSObject <YPSFileComparator>
@end

/** test file contents for byte-for-byte equality */
@interface YPSFileContentsMatcher : NSObject <YPSFileComparator>
@end