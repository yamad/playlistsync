//
//  main.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/15/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Cocoa;

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
