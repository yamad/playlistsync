//
//  ViewController.m
//  Shangri-La Playlists
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "MainViewController.h"

#import "AppDelegate.h"
#import "YPSPlaylistSync.h"
#import "constants.h"

@implementation MainViewController {
    NSMutableArray *_observers;
    id _progressStartObserver;
    id _progressStopObserver;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _progressIndicator.hidden = YES;

    _observers = [NSMutableArray array];
    __weak __typeof(self) weakSelf = self;
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    id countOb = [nc addObserverForName:kYPSStartCountdownTickNotification
                         object:nil
                          queue:[NSOperationQueue mainQueue]
                     usingBlock:^(NSNotification *note) {
                         __strong __typeof(weakSelf) strongSelf = weakSelf;
                         [strongSelf handleStartCountdownTick:note];
                     }];
    [_observers addObject:countOb];

    id syncStartOb = [nc addObserverForName:kYPSPlaylistSyncStartNotification
                         object:nil
                          queue:[NSOperationQueue mainQueue]
                     usingBlock:^(NSNotification *note) {
                         __strong __typeof(weakSelf) strongSelf = weakSelf;
                         [strongSelf handleSyncStart:note];
                     }];
    [_observers addObject:syncStartOb];

    id syncStopOb = [nc addObserverForName:kYPSPlaylistSyncStopNotification
                         object:nil
                          queue:[NSOperationQueue mainQueue]
                     usingBlock:^(NSNotification *note) {
                         __strong __typeof(weakSelf) strongSelf = weakSelf;
                         [strongSelf handleSyncStop:note];
                     }];
    [_observers addObject:syncStopOb];

    id appLoadOb = [nc addObserverForName:kYPSApplicationFinishedLaunchNotification
                                   object:nil
                                    queue:[NSOperationQueue mainQueue]
                               usingBlock:^(NSNotification *note) {
                                   [self fillFromPreferences];
                               }];
    [_observers addObject:appLoadOb];
}

- (void)viewDidDisappear {
    [super viewDidDisappear];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (id o in _observers) {
        [nc removeObserver:o];
    }

    [nc removeObserver:_progressStartObserver];
    [nc removeObserver:_progressStopObserver];
}

- (void)setRepresentedObject:(id)representedObject {
    super.representedObject = representedObject;

    // Update the view, if already loaded.
}

- (IBAction)toggleAutoRestart:(NSButton *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:_autoRestartSelect.state
                                            forKey:kYPSShouldAutoRestartKey];
}

- (IBAction)toggleTrackFileDeletions:(NSButton *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:_trackDeletionsSelect.state
                                            forKey:kYPSTrackDeletionsKey];
}

- (IBAction)chooseRootFolder:(NSPathCell *)sender {
    AppDelegate *aD = [[NSApplication sharedApplication] delegate];
    [aD stopTimedStartFolderSync]; // stop any countdown timers
    [self syncStopUIState];

    NSURL *newURL = [sender.clickedPathComponentCell.URL URLByStandardizingPath];
    if (aD.playlistSync) {
        // changing root folder, so ask for confirmation
        if (![aD.playlistSync.rootURL isEqual:newURL]) {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert addButtonWithTitle:@"OK"];
            [alert addButtonWithTitle:@"Cancel"];
            [alert setMessageText:@"Really change watch folder?"];
            [alert setInformativeText:
             [NSString stringWithFormat:@"Changing folder to %@", newURL.path]];

            if ([alert runModal] == NSAlertSecondButtonReturn) {
                // cancel clicked, reset to known watch folder
                sender.URL = aD.playlistSync.rootURL;
                return;
            }
        }
    }
    // no folder already selected or confirmed change
    [aD setupFolderSync:newURL];
    [aD startTimedStartFolderSync];
    sender.URL = newURL;
}

- (IBAction)updateDefaultPlaylistName:(NSTextField *)sender {
    if (sender.stringValue.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:sender.stringValue
                                                  forKey:kYPSDefaultPlaylistNameKey];
    } else {
        sender.stringValue = [[NSUserDefaults standardUserDefaults] stringForKey:kYPSDefaultPlaylistNameKey];
    }
}

- (IBAction)selectStartButton:(NSButton *)sender {
    AppDelegate *aD = [[NSApplication sharedApplication] delegate];
    if (!aD.playlistSync.isRunning) {
        [aD setupFolderSync:_rootFolderSelector.URL];
        [aD startFolderSync];
        [self syncStartUIState];
    }
}

- (IBAction)selectStopButton:(NSButton *)sender {
    AppDelegate *aD = [[NSApplication sharedApplication] delegate];
    if (aD.playlistSync.isRunning) {
        [aD stopFolderSync];
        // UI state is made consistent by notification from YPSPlaylistSync
    } else {
        [aD stopTimedStartFolderSync];
        [self syncStopUIState];
    }
}

- (void) syncStartUIState {
    _stopButton.enabled = YES;
    _startButton.title = @"Start";
    [_startButton highlight:NO];
    _startButton.enabled = NO;
    _progressIndicator.hidden = NO;
}

- (void) syncStopUIState {
    _startButton.enabled = YES;
    _startButton.title = @"Start";
    [_startButton highlight:NO];
    _stopButton.enabled = NO;
    _progressIndicator.hidden = YES;
}

- (void) fillFromPreferences {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSURL *rootURL = nil;
    // set root URL field
    if ([ud objectForKey:kYPSRootFolderURLKey]) {
        rootURL = [ud URLForKey:kYPSRootFolderURLKey];
        [_rootFolderSelector setURL:rootURL];
    }

    // set restart button from preferences
    BOOL autoRestart = [ud boolForKey:kYPSShouldAutoRestartKey];
    _autoRestartSelect.state = autoRestart;

    // set track deletions from preferences
    BOOL trackDeletions = [ud boolForKey:kYPSTrackDeletionsKey];
    _trackDeletionsSelect.state = trackDeletions;

    // set default playlist from preferences
    NSString *defaultName;
    if ([ud objectForKey:kYPSDefaultPlaylistNameKey]) {
        defaultName = [ud stringForKey:kYPSDefaultPlaylistNameKey];
        if (defaultName.length > 0) {
            _defaultPlaylistField.stringValue = defaultName;
        } else {
            // reset to registered default if invalid
            // TODO: put this somewhere where it makes more sense
            NSDictionary *regD = [ud volatileDomainForName:NSRegistrationDomain];
            defaultName = regD[kYPSDefaultPlaylistNameKey]; // get registered name
            _defaultPlaylistField.stringValue = defaultName;
            [ud setObject:defaultName forKey:kYPSDefaultPlaylistNameKey];
        }
    }
}

#pragma mark - Notification handlers
- (void) handleStartCountdownTick:(NSNotification *) note {
    _startButton.title = [NSString stringWithFormat:@"Auto-start in %@ seconds...",
                          note.userInfo[@"secondsLeft"]];
    [_startButton highlight:YES];
    _stopButton.enabled = YES;
}

- (void) handleSyncStart:(NSNotification *) note {
    [self syncStartUIState];
    [_statusMessage setStringValue:@"Watching..." ];

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    __weak __typeof(self) weakSelf = self;
    _progressStartObserver = [nc addObserverForName:kYPSStartProgressOperationNotification
                                             object:nil
                                              queue:[NSOperationQueue mainQueue]
                                         usingBlock:^(NSNotification *note) {
                                             __strong __typeof(weakSelf) strongSelf = weakSelf;
                                             [strongSelf handleStartProgress:note];
                                         }];

    _progressStopObserver = [nc addObserverForName:kYPSStopProgressOperationNotification
                                            object:nil
                                             queue:[NSOperationQueue mainQueue]
                                        usingBlock:^(NSNotification *note) {
                                            __strong __typeof(weakSelf) strongSelf = weakSelf;
                                            [strongSelf handleStopProgress:note];
                                        }];
}

- (void) handleSyncStop:(NSNotification *) note {
    [self syncStopUIState];
    [_progressIndicator stopAnimation:nil];

    if (note.userInfo[@"message"])
        _statusMessage.stringValue = note.userInfo[@"message"];
    else
        _statusMessage.stringValue = @"";

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    if (_progressStartObserver) {
        [nc removeObserver:_progressStartObserver];
        _progressStartObserver = nil;
    }
    if (_progressStopObserver) {
        [nc removeObserver:_progressStopObserver];
        _progressStopObserver = nil;
    }
}

- (void) handleStartProgress:(NSNotification *) note {
    if (note.userInfo[@"message"])
        _statusMessage.stringValue = note.userInfo[@"message"];
    [_progressIndicator startAnimation:nil];
}

- (void) handleStopProgress:(NSNotification *) note {
    _statusMessage.stringValue = @"Watching...";
    [_progressIndicator stopAnimation:nil];
}

@end
