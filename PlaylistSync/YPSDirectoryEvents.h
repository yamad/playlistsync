//
//  YPSDirectoryEvents.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/20/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

#import "YPSDirectorySnapshot.h"
#import "YPSDirectoryComparator.h"

@class CDEvent;

/** Delegate handling of each event */
@protocol YPSDirectoryEventsDelegate
- (void) handleEvent:(CDEvent *)event;
@end

/** Directory events interface */
@interface YPSDirectoryEvents : NSObject
@property (copy, readonly) NSURL *watchedURL;
@property (readonly) NSMutableDictionary<NSURL *, CDEvent *> *lastEventByPath;
@property (readonly) NSMutableDictionary<NSURL *, YPSDirectorySnapshot *> *lastDirSnapByPath;

- (instancetype)initWithURL:(NSURL *)watchedURL;
- (instancetype)initWithURL:(NSURL *)watchedURL
                   delegate:(id<YPSDirectoryEventsDelegate>)delegate;
- (instancetype)initWithURL:(NSURL *)watchedURL
                   delegate:(id<YPSDirectoryEventsDelegate>)delegate
           startImmediately:(BOOL)startImmediately NS_DESIGNATED_INITIALIZER;

/*! @abstract Use initWithURL:... instead */
-(instancetype) init NS_UNAVAILABLE;

- (void) start;
- (void) stop;
- (void) restart;
@end


/** Name for notifications of each event */
extern NSString *const kYPSDirectoryEventNotification;
