//
//  YPSPlaylist.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/22/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@class YPSPlaylistManager;

@interface YPSPlaylist : NSObject
@property NSString *tag;
@property NSString *name;
@property (weak) YPSPlaylistManager *manager; // parent manager

// holds stuff to do to sync changes to iTunes
@property NSMutableSet<NSURL *> *filesToAdd;
@property NSString *renameName;

- (BOOL) addFileAtURL:(NSURL *)fileURL;
- (BOOL) rename:(NSString *)newName;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *display;

- (instancetype) initWithTag:(NSString*)tag
                     andName:(NSString*)name
                    andFiles:(NSSet<NSURL *> *)fileURLs NS_DESIGNATED_INITIALIZER;
- (instancetype) initWithTag:(NSString*)tag;
- (instancetype) initWithTag:(NSString*)tag andFile:(NSURL *)fileURL;
- (instancetype) initWithTag:(NSString*)tag andName:(NSString*)name;

/*! @abstract Use initWithTag:... instead */
-(instancetype) init NS_UNAVAILABLE;

@end