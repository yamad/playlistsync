//
//  constants.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/29/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

#pragma mark - Application-wide values
extern NSString *const kYPSDefaultPlaylistTag;       // "tag" for default playlist
extern NSString *const kYPSTagRegex;

#pragma mark - Preference Keys
extern NSString *const kYPSRootFolderURLKey;         // root folder to watch
extern NSString *const kYPSPlaylistNameExcelFileKey; // path to excel spreadsheet with playlist names
extern NSString *const kYPSDefaultPlaylistNameKey;   // name of default/catchall playlist
extern NSString *const kYPSShouldAutoRestartKey;     // automatically restart sync on program start
extern NSString *const kYPSTrackDeletionsKey;        // track file deletions in watch folder
extern NSString *const kYPSTrackChangesKey;          // track file changes in watch folder

#pragma mark - Notifications
extern NSString *const kYPSStartCountdownTickNotification;
extern NSString *const kYPSPlaylistSyncStartNotification;
extern NSString *const kYPSPlaylistSyncStopNotification;
extern NSString *const kYPSStartProgressOperationNotification;
extern NSString *const kYPSStopProgressOperationNotification;
extern NSString *const kYPSApplicationFinishedLaunchNotification;
extern NSString *const kYPSNewTagDiscoveredNotification;