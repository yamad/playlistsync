//
//  YPSPlaylistTableViewController.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/9/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class YPSPlaylistTableViewModel;
@class YPSTagCellView;

@interface YPSPlaylistTableViewController : NSViewController <NSTableViewDelegate, NSTableViewDataSource>
@property (weak) IBOutlet NSTableView *tableView;
@property (nonatomic, strong) YPSPlaylistTableViewModel *viewModel;
@property (weak) IBOutlet NSSegmentedControl *addRemoveButtons;

- (IBAction)editTag:(NSTextFieldCell *)sender;
- (IBAction)editName:(NSTextFieldCell *)sender;
- (IBAction)chooseAddOrRemove:(NSSegmentedControl *)sender;
- (instancetype) initWithViewModel:(YPSPlaylistTableViewModel *)viewModel;
@end
