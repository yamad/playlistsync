//
//  AppDelegate.m
//  Shangri-La Playlists
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "AppDelegate.h"
#import "YPSPlaylistSync.h"

#import "YPSPlaylistNameProvider.h"

#import "logging.h"
#import "constants.h"

@interface AppDelegate ()
@property NSMutableArray *observers;

@end

@implementation AppDelegate {
    NSInteger _timerCount;
    NSTimer *_timer;
    dispatch_queue_t _sharedQueue;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self registerDefaultPreferences];
    [self configureLogging];
    _sharedQueue = dispatch_queue_create("com.jyh.playlistsync.globalserialqueue", DISPATCH_QUEUE_SERIAL);
    _nameProvider = [[YPSInternalNameProvider alloc] init];

    // (optionally) start synchronzining automatically
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kYPSShouldAutoRestartKey] &&
        [[NSUserDefaults standardUserDefaults] objectForKey:kYPSRootFolderURLKey]) {
        NSURL *rootURL = [[NSUserDefaults standardUserDefaults] URLForKey:kYPSRootFolderURLKey];

        [self setupFolderSync:rootURL];
        [self startTimedStartFolderSync];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kYPSApplicationFinishedLaunchNotification
                                                        object:self userInfo:nil];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (id o in _observers) {
        [nc removeObserver:o];
    }
    [_observers removeAllObjects];

    [_playlistSync stop];
    [_nameProvider stop];
}

- (BOOL) applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    // make sure window always opens when application is selected
    if (flag) {
        [[[[NSApplication sharedApplication] windows] firstObject] orderFront:self];
    } else {
        [[[[NSApplication sharedApplication] windows] firstObject] makeKeyAndOrderFront:self];
    }
    return YES;
}

- (void) setupFolderSync:(NSURL *)selectedURL {
    if (_playlistSync)
        [_playlistSync stop];


    selectedURL = [selectedURL URLByStandardizingPath];
    _playlistSync = [[YPSPlaylistSync alloc] initWithRootURL:selectedURL
                                             backgroundQueue:_sharedQueue
                                                nameProvider:_nameProvider
                                            startImmediately:NO];
    if (_playlistSync)
        [[NSUserDefaults standardUserDefaults] setURL:selectedURL
                                               forKey:kYPSRootFolderURLKey];
}

- (void) startFolderSync {
    if (_timer)
        [self stopTimedStartFolderSync];
    [_playlistSync start];
}

- (void) stopFolderSync {
    [_playlistSync stop];
}

- (void) startTimedStartFolderSync {
    _timerCount = 15;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                              target:self
                                            selector:@selector(countdownStartSync:)
                                            userInfo:nil
                                             repeats:YES];
}

- (void) stopTimedStartFolderSync {
    [_timer invalidate];
    _timer = nil;
    _timerCount = 0;
}

- (void) countdownStartSync:(NSTimer*) sender {
    _timerCount--;

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartCountdownTickNotification
     object:self
     userInfo:@{ @"secondsLeft" : @(_timerCount) }];

    if (_timerCount < 0) {
        [self stopTimedStartFolderSync];
        [self startFolderSync];
    }
}

#pragma mark - Application Setup
- (void) registerDefaultPreferences {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kYPSTrackChangesKey];
    NSDictionary* defaults = @{kYPSDefaultPlaylistNameKey:@"CATCH ALL",
                               kYPSShouldAutoRestartKey:@YES,
                               kYPSTrackChangesKey:@YES,
                               kYPSTrackDeletionsKey:@NO };
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];

}

- (void) configureLogging {
    // Configure CocoaLumberjack
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];

    // Initialize File Logger
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];

    // Configure File Logger
    fileLogger.maximumFileSize = (1024 * 1024 * 5);
    fileLogger.rollingFrequency = (3600.0 * 24.0);
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    [DDLog addLogger:fileLogger];
}

@end
