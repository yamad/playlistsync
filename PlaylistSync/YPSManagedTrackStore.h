//
//  YPSManagedTrackStore.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/9/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YPSManagedTrack;

@interface YPSManagedTrackStore : NSObject
- (YPSManagedTrack *) getTrackAtFileURL:(NSURL *)fileURL;
- (BOOL) hasTrackAtURL:(NSURL *)fileURL;
- (void) removeTrackAtURL:(NSURL *)fileURL;
- (NSUInteger) count;

- (BOOL) addTrackWithURL:(NSURL*)fileURL andDbID:(NSNumber*)dbID andITunesURL:(NSURL*)itURL checkFileMatch:(BOOL)check;
- (BOOL) addTrackWithURL:(NSURL*)fileURL andDbID:(NSNumber*)dbID andITunesURL:(NSURL*)itURL;
@end
