//
//  YPSPlaylistTableViewController.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/9/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "AppDelegate.h"

#import "YPSPlaylistTableViewController.h"

#import "YPSPlaylistTableViewModel.h"
#import "YPSPlaylistViewModel.h"

#import "constants.h"

@interface YPSPlaylistTableViewController ()
@property NSMutableArray *observers;
@end

@implementation YPSPlaylistTableViewController

- (IBAction)editTag:(NSTextFieldCell *)sender {
    self.addRemoveButtons.enabled = YES;
    NSInteger selectedRow = self.tableView.selectedRow;
    YPSPlaylistViewModel *pvm = self.viewModel.playlists[selectedRow];

    pvm.isValid = [self.viewModel validateTag:sender.stringValue];
    pvm.tag = sender.stringValue;

    NSTableCellView *cell = [self.tableView viewAtColumn:0 row:selectedRow makeIfNecessary:NO];
    if (cell) {
        if (!pvm.isValid) {
            cell.imageView.image = [NSImage imageNamed:NSImageNameStatusUnavailable];
        } else {
            cell.imageView.image = [NSImage imageNamed:NSImageNameStatusNone];
        }
    }

    // name is already set, so save
    if (pvm.name.length > 0) {
        [self.viewModel syncToModel];
        [self.tableView reloadData];
    }
}

- (IBAction)editName:(NSTextFieldCell *)sender {
    self.addRemoveButtons.enabled = YES;
    NSInteger selectedRow = self.tableView.selectedRow;

    YPSPlaylistViewModel *pvm = self.viewModel.playlists[selectedRow];
    pvm.name = sender.stringValue;
    [self.viewModel syncToModel];
    [self.tableView reloadData];
}

- (IBAction)chooseAddOrRemove:(NSSegmentedControl *)sender {
    NSInteger clickedSegment    = [sender selectedSegment];
    NSInteger clickedSegmentTag = [[sender cell] tagForSegment:clickedSegment];
    if (clickedSegmentTag == 0) {
        // add button pressed. add playlist view model and edit
        NSInteger idx = [self.viewModel addNewPlaylist];
        self.addRemoveButtons.enabled = NO;
        [self.tableView reloadData];
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:idx]
                    byExtendingSelection:NO];
        [self.tableView editColumn:0 row:idx withEvent:nil select:YES];
    } else if (clickedSegmentTag == 1) {
        // remove button pressed. remove selected playlist, if any
        NSInteger selectedRow = self.tableView.selectedRow;
        if (selectedRow > -1) {
            if ([self.viewModel removePlaylistAtIndex:selectedRow])
                [self.tableView reloadData];
        }
    }
}

- (instancetype) initWithViewModel:(YPSPlaylistTableViewModel *)viewModel {
    self = [super init];
    if (self) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];

    self.viewModel = [[YPSPlaylistTableViewModel alloc] init];

    //  __weak __typeof(self) weakSelf = self;
    // deal with possible out-of-order calls to application delegates
    // the application's shared name provider must be set before setting
    // the view model
    [[NSNotificationCenter defaultCenter]
     addObserverForName:kYPSApplicationFinishedLaunchNotification
     object:[[NSApplication sharedApplication] delegate]
     queue:[NSOperationQueue mainQueue]
     usingBlock:^(NSNotification *note) {
         // bind model. would rather do it top-down, but haven't been able to
         // construct windows/views top-down when the Storyboard wants to control it.
         //       __strong __typeof(self) strongSelf = weakSelf;
         AppDelegate *aD = [[NSApplication sharedApplication] delegate];
         self.viewModel.nameProvider = aD.nameProvider;
         [self.tableView reloadData];

         id o = [[NSNotificationCenter defaultCenter]
                 addObserverForName:kYPSNewTagDiscoveredNotification
                 object:self.viewModel
                 queue:[NSOperationQueue mainQueue]
                 usingBlock:^(NSNotification *note) {
                     [self.tableView reloadData];
                 }];
         [_observers addObject:o];
     }];
}

- (void) viewDidDisappear {
    [super viewDidDisappear];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (id o in _observers) {
        [nc removeObserver:o];
    }
    [_observers removeAllObjects];
}

#pragma mark - YPSTableViewSource
- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
    return self.viewModel.playlists.count;
}

#pragma mark - YPSTableViewDelegate
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {

    if (row >= self.viewModel.playlists.count) return nil;

    YPSPlaylistViewModel *pvm = self.viewModel.playlists[row];
    NSString *cellID;
    NSString *cellValue;
    BOOL isEditable = YES;

    if (tableColumn == tableView.tableColumns[0]) {
        cellID = @"PlaylistTable.Tag";
        cellValue = pvm.tag;
        if (!pvm.isNew)
            isEditable = NO;

    } else if (tableColumn == tableView.tableColumns[1]) {
        cellID = @"PlaylistTable.Name";
        cellValue = pvm.name;
    }

    NSTableCellView *cell = [tableView makeViewWithIdentifier:cellID
                                                        owner:self];
    cell.textField.stringValue = cellValue;
    [cell.textField setEditable:isEditable];

    NSImage *img = nil;
    if (pvm.isNew)
        img = [NSImage imageNamed:NSImageNameStatusNone];
    if (!pvm.isValid && pvm.isDirty)
        img = [NSImage imageNamed:NSImageNameStatusUnavailable];
    cell.imageView.image = img;
    return cell;
}

@end
