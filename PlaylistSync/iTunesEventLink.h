//
//  iTunesEventLink.h
//  iTunesEventLink
//
//  Created by Jason Yamada-Hanff on 5/15/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@class ITunesEventLink;

// result from iTunes link. always has `status` holding an ITunesStatus.
// results with ITunesStatus_OK have a `value` field holding payload
// results with an error ITunesStatus have `code` and `message` fields
typedef NSDictionary ITunesResult;

// status codes appearing in `status` field of ITunesResult
// (check against applescript source. HAVE TO MANUALLY ALIGN)
typedef NS_ENUM(NSInteger, ITunesStatus) {
    ITunesStatus_AppleScriptError = -2,
    ITunesStatus_Error = -1,
    ITunesStatus_OK = 0
};

// result codes appearing in `code` or `value` field of ITunesResult
// (check against applescript source. HAVE TO MANUALLY ALIGN)
typedef NS_ENUM(NSInteger, ITunesResultCode) {
    ITunesResultCode_Unspecified = 0,
    ITunesResultCode_NotOpen = 1,
    ITunesResultCode_PlaylistExists = 2,
    ITunesResultCode_PlaylistCreated = 3,
    ITunesResultCode_FileDoesNotExist = 4,
    ITunesResultCode_PlaylistDoesNotExist = 5,
    ITunesResultCode_TrackIDDoesNotExist = 6,
    ITunesResultCode_TrackExists = 7,
    ITunesResultCode_TrackAdded = 8,
    ITunesResultCode_TrackIsPlaylistDuplicate = 9,
    ITunesResultCode_FileAdded = 10,
    ITunesResultCode_FileRejected = 11,
    ITunesResultCode_TrackRemoved = 12,
    ITunesResultCode_TrackDoesNotMatch = 13,
    ITunesResultCode_NoTracks = 14
};

@interface ITunesEventLink : NSObject
+ (ITunesResult *) addTrackID:(NSNumber*)dbID toPlaylist:(NSString*)plName;
+ (ITunesResult *) renamePlaylistFrom:(NSString*)oldName to:(NSString*)newName;
+ (ITunesResult *) addFileWithoutChecks:(NSString *)filePath toPlaylist:(NSString *)plName;
+ (ITunesResult *) removeTrackByID:(NSNumber*)dbID andFile:(NSString *)filePath;
+ (ITunesResult *) getAllFileTrackInfo;
+ (ITunesResult *) getAllTracksFromPlaylist:(NSString *)plName;
+ (ITunesResult *) addFileList:(NSArray<NSString*> *)filePaths toPlaylist:(NSString*)plName;
@end