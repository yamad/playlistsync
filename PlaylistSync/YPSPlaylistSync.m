//
//  YPSPlaylistSync.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/21/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <AppleScriptObjC/AppleScriptObjC.h>

#import "YPSPlaylistSync.h"

#import "YPSDirectorySnapshot.h"
#import "YPSDirectoryComparator.h"
#import "YPSPlaylistManager.h"
#import "ITunesEventLink.h"
#import "YPSDirectoryEvents.h"
#import "YPSPlaylistNameProvider.h"
#import "YPSFileComparator.h"
#import "YPSManagedTrack.h"
#import "CDEvent.h"

#import "logging.h"
#import "constants.h"


NSComparator cmpLastPaths = ^NSComparisonResult(NSDictionary *d1, NSDictionary *d2) {
    NSString *fname1 = [d1[@"location"] lastPathComponent];
    NSString *fname2 = [d2[@"location"] lastPathComponent];
    return [fname1 compare:fname2];
};

NSComparator cmpStubs = ^NSComparisonResult(NSDictionary *d1, NSDictionary *d2) {
    if ([YPSFilenameStubMatcher matchAtURL:d2[@"location"] andURL:d1[@"location"] error:nil])
        return NSOrderedSame;
    else
        return cmpLastPaths(d1, d2);
};


@interface YPSPlaylistSync () <YPSDirectoryEventsDelegate, YPSDirectoryComparatorChangeDelegate>   {
    YPSDirectorySnapshot *_lastSnapshot;
    dispatch_queue_t _queue;
}
@property YPSDirectoryEvents *eventsWatcher;
- (BOOL) hasCargoDownloadProgressFile:(NSURL*)fileURL;
- (BOOL) isCargoDownloadProgressFile:(NSURL*)fileURL;
@end


@implementation YPSPlaylistSync

- (instancetype) initWithRootURL:(NSURL *)rootURL
{
    return [self initWithRootURL:rootURL startImmediately:YES];
}

- (instancetype) initWithRootURL:(NSURL *)rootURL startImmediately:(BOOL)startImmediately {
    return [self initWithRootURL:rootURL
                 backgroundQueue:dispatch_queue_create("com.jyh.playlistsync.psqueue", DISPATCH_QUEUE_SERIAL)
                    nameProvider:[[YPSPlaylistNameProvider alloc] init]
                startImmediately:startImmediately];
}

- (instancetype) initWithRootURL:(NSURL *)rootURL
                 backgroundQueue:(dispatch_queue_t)queue
                    nameProvider:(YPSPlaylistNameProvider *)nameProvider
                startImmediately:(BOOL)startImmediately
{
    NSAssert(rootURL != nil, @"root path/url must be non-nil");
    if ((self = [super init])) {
        _rootURL         = [rootURL copy];

        _playlistManager = [[YPSPlaylistManager alloc] initWithNameProvider:nameProvider];

        _problemFiles    = [NSMutableSet set];
        _ignoredFiles    = [NSMutableSet set];
        _eventsWatcher   = [[YPSDirectoryEvents alloc] initWithURL:_rootURL
                                                          delegate:self
                                                  startImmediately:NO];
        _lastSnapshot    = [[YPSDirectorySnapshot alloc] initWithURL:_rootURL];
        _isRunning       = NO;

        // for each method that calls iTunes, get a new instance with:
        //   id iTLink = [NSClassFromString(@"ITunesEventLink") new];
        //    ... method body ...
        //    iTLink = nil;
        //[[NSBundle mainBundle] loadAppleScriptObjectiveCScripts];
        [[NSBundle bundleForClass:[self class]] loadAppleScriptObjectiveCScripts];

        _trackStore = [[YPSManagedTrackStore alloc] init];
        _queue = queue;
        if (startImmediately)
            [self start];
    }
    return self;
}

- (BOOL) start {
    // build initial state in background thread. avoids blocking UI
    if (_isRunning)
        return NO;

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSPlaylistSyncStartNotification
     object:self];
    _isRunning = YES;

    DDLogInfo(@"Start watching folder %@", _rootURL.path);
    // do expensive operations off of the main thread
    // put on a globally shared serial queue, so that restarts don't crash iTunes
    // the methods must run in order
    [self.eventsWatcher start];
    dispatch_async(_queue, ^(){
        [self retrieveiTunesState];
        [self setupFullSync];
        [self syncWithiTunes];
    });

    return YES;
}

- (BOOL) stop {
    if (!_isRunning) return NO;

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartProgressOperationNotification
     object:self
     userInfo:@{@"message" : @"Shutting down sync operations..."}];

    [_eventsWatcher stop];
    _isRunning = NO;

    DDLogInfo(@"Stop watching folder %@", _rootURL.path);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSPlaylistSyncStopNotification
     object:self];

    return YES;
}

- (BOOL) addFile:(NSURL *)fileURL {
    if ([self isManageableFileType:fileURL]) {
        DDLogInfo(@"File added: %@", [fileURL path]);
        [_playlistManager addFileAtURL:fileURL error:nil];
    } else {
        DDLogInfo(@"File added ignored (not managed): %@", [fileURL path]);
        [_ignoredFiles addObject:fileURL];
        return NO;
    }
    return YES;
}

- (BOOL) removeChangedFile:(NSURL *)fileURL {
    if ([self isManageableFileType:fileURL]) {
        DDLogInfo(@"File removal for change: %@", [fileURL path]);
        [_playlistManager removeChangedFileAtURL:fileURL error:nil];
    } else {
        DDLogInfo(@"File change removal ignored (not managed): %@", [fileURL path]);
        [_ignoredFiles addObject:fileURL];
        return NO;
    }
    return YES;
}

- (BOOL) removeFile:(NSURL *)fileURL {
    if ([self isManageableFileType:fileURL]) {
        DDLogInfo(@"File removal: %@", [fileURL path]);
        [_playlistManager removeFileAtURL:fileURL error:nil];
    } else {
        DDLogInfo(@"File removal ignored (not managed): %@", [fileURL path]);
        [_ignoredFiles addObject:fileURL];
        return NO;
    }
    return YES;
}

- (BOOL) setupFullSync {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Preparing full directory..." }];

    [_problemFiles removeAllObjects];
    [_ignoredFiles removeAllObjects];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSDirectoryEnumerator *dirContents = [fm enumeratorAtURL:_rootURL
                                  includingPropertiesForKeys:0
                                                     options:NSDirectoryEnumerationSkipsHiddenFiles
                                                errorHandler:nil];

    BOOL isDir;
    for (NSURL *fileURL in dirContents) {
        // skip directories
        if ([fm fileExistsAtPath:fileURL.path isDirectory:&isDir] && isDir)
            continue;

        // add or re-add every file.
        // don't check against known tracks because these might be out of date
        [self addFile:fileURL];

    }

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStopProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Done preparing directory..." }];

    return YES;
}

// TODO: refactor and delegate to playlists
- (BOOL) syncWithiTunes {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Syncing with iTunes..." }];

    DDLogInfo(@"Synchronizing changes with iTunes...");

    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:kYPSTrackDeletionsKey]) {
        DDLogInfo(@"   removing files:");
        // remove files
        for (NSURL *fileURL in _playlistManager.filesToRemove) {
            DDLogInfo(@"Removing track from iTunes: %@ ...", fileURL);
            if ([self removeFileFromiTunes:fileURL]) {
                DDLogInfo(@"    success");
            } else {
                DDLogInfo(@"    failed");
            }
        }
    }
    [_playlistManager.filesToRemove removeAllObjects];


    if ([ud boolForKey:kYPSTrackChangesKey]) {
        DDLogInfo(@"   changed files:");
        // remove changed files, will be re-added with changes
        for (NSURL *fileURL in _playlistManager.filesChanged) {
            DDLogInfo(@"Removing track from iTunes: %@ ...", fileURL);
            if ([self removeFileFromiTunes:fileURL]) {
                DDLogInfo(@"    success");
            } else {
                DDLogInfo(@"    failed");
            }
        }
    }
    [_playlistManager.filesChanged removeAllObjects];

    DDLogInfo(@"   adding files by playlist:");
    // sync default playlist first
    [self syncPlaylistWithiTunes:kYPSDefaultPlaylistTag];

    // iterate by playlist
    for (NSString *plTag in _playlistManager.playlistsByTag) {
        if (![plTag isEqual:kYPSDefaultPlaylistTag])
            [self syncPlaylistWithiTunes:plTag];
    }

    DDLogInfo(@"Done synchronizing with iTunes");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStopProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Done syncing with iTunes..." }];

    return YES;
}

- (void)renamePlaylist:(YPSPlaylist *)pl iTLink:(id)iTLink {
    DDLogInfo(@"  Renaming playlist %@ to %@", pl.name, pl.renameName);

    ITunesResult *itr = [iTLink renamePlaylistFrom:pl.name to:pl.renameName];

    // iTunes link failed. can't continue
    if (!itr) {
        [self handleITunesNoResponseError];
        return;
    }

    ITunesStatus status   = [itr[@"status"] integerValue];
    ITunesResultCode code = [itr[@"code"]   integerValue];
    if (status == ITunesStatus_OK) {
        pl.name = pl.renameName;
        [pl setRenameName:nil];

        // log the results
        DDLogDebug(@"... successfully renamed playlist id %@", itr[@"value"]);
    } else if (status == ITunesStatus_Error &&
               code   == ITunesResultCode_PlaylistDoesNotExist) {
        DDLogDebug(@"... failed. playlist %@ does not exist", itr[@"message"]);
    } else {
        DDLogDebug(@"... failed: (%@) %@", itr[@"code"], itr[@"message"]);
    }
}

- (void) syncPlaylistWithiTunes:(NSString *)playlistTag {
    @autoreleasepool {
    id iTLink = [NSClassFromString(@"ITunesEventLink") new];
    ITunesResult *itr;
    ITunesStatus itrStatus;

    YPSPlaylist *pl = _playlistManager.playlistsByTag[playlistTag];

    // is there anything to do
    if ((!pl.filesToAdd || [pl.filesToAdd count] == 0) && !pl.renameName)
        return;

    DDLogInfo(@"Updating playlist (%@) %@", pl.tag, pl.name);

    // rename playlist, if desired
    if (pl.renameName) {
        [self renamePlaylist:pl iTLink:iTLink];
    }

    NSArray *plDbIDs = @[];
    NSArray *plLocations = @[];
    itr = [iTLink getAllTracksFromPlaylist:pl.name];
    if (!itr) {
        [self handleITunesNoResponseError];
        return;
    }

    // TODO: get files by playlist is not working
    itrStatus = [itr[@"status"] integerValue];
    if (itrStatus == ITunesStatus_OK) {
        plDbIDs = itr[@"value"][@"dbID"];
        plLocations = itr[@"value"][@"location"];
    }

    NSUInteger filesProcessed = 0;
    // add all new files

    // sort by modification date
    NSArray *sortedFiles = @[];
    if (pl.filesToAdd && ([pl.filesToAdd count] > 0)){
        sortedFiles = [[pl.filesToAdd allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDate *mDate1 = nil;
            NSDate *mDate2 = nil;
            if ([(NSURL*)obj1 getResourceValue:&mDate1 forKey:NSURLContentModificationDateKey error:nil] &&
                [(NSURL*)obj2 getResourceValue:&mDate2 forKey:NSURLContentModificationDateKey error:nil]) {
                if ([mDate1 timeIntervalSince1970] < [mDate2 timeIntervalSince1970]) return NSOrderedAscending;
                else                                                                 return NSOrderedDescending;
            }
            return NSOrderedSame; // there was an error in getting the value
        }];
    }

    for (NSURL *fileURL in sortedFiles) {
        // check if we already have the file in iTunes
        YPSManagedTrack *mt = [_trackStore getTrackAtFileURL:fileURL];
        if (mt) {
            if ([plDbIDs containsObject:mt.itunesDatabaseID] &&
                [plLocations containsObject:mt.itunesURL]) {
                DDLogInfo(@"Existing track id %@ and file %@ is already in playlist %@",
                          mt.itunesDatabaseID, mt.fileURL.path, pl.name);
                continue;
            }

            DDLogInfo(@"Adding existing track id %@ with file %@ to playlist %@",
                      mt.itunesDatabaseID, mt.fileURL.path, pl.name);
            itr = [iTLink addTrackID:mt.itunesDatabaseID toPlaylist:pl.name];
            if (!itr) {
                [self handleITunesNoResponseError];
                return;
            }

            // log the results
            ITunesStatus status = [itr[@"status"] integerValue];
            if (status == ITunesStatus_OK) {
                if ([itr[@"code"] isEqual: @(ITunesResultCode_TrackIsPlaylistDuplicate)]) {
                    DDLogDebug(@"... track was already in playlist");
                } else if ([itr[@"code"] isEqual: @(ITunesResultCode_TrackAdded)]) {
                    DDLogDebug(@"... track was added to playlist");
                }
            } else {
                DDLogDebug(@"... failed. (%@) %@", itr[@"code"], itr[@"message"]);
                [_problemFiles addObject:fileURL];
            }
        } else {
            DDLogInfo(@"Adding new file %@ to playlist %@", fileURL.path, pl.name);
            if (![fileURL checkResourceIsReachableAndReturnError:nil]) { // check file exists
                DDLogDebug(@"... file does not exist: %@", fileURL.path);
            } else {
                itr = [iTLink addFileWithoutChecks:fileURL.path toPlaylist:pl.name];
                if (!itr) {
                    [self handleITunesNoResponseError];
                    return;
                }

                ITunesStatus status = [itr[@"status"] integerValue];
                if (status == ITunesStatus_OK) {
                    NSDictionary *itTrack = itr[@"value"];
                    [_trackStore addTrackWithURL:fileURL
                                         andDbID:itTrack[@"dbID"]
                                    andITunesURL:itTrack[@"location"]];
                    DDLogDebug(@"... succesfully added as new track ID %@", itTrack[@"dbID"]);
                } else {
                    DDLogDebug(@"... failed. (%@) %@", itr[@"code"], itr[@"message"]);
                    [_problemFiles addObject:fileURL];
                }
            }
        }
        // refresh iTunes link to guard against Applescript memory barriers
        if (filesProcessed++ > 50) {
            iTLink = [NSClassFromString(@"ITunesEventLink") new];
            filesProcessed = 0;
        }

    }
    [pl.filesToAdd removeAllObjects];
    }
}

- (BOOL) removeFileFromiTunes:(NSURL *)fileURL {
    id iTLink = [NSClassFromString(@"ITunesEventLink") new];
    // make sure we have a record for the track
    YPSManagedTrack *mt = [_trackStore getTrackAtFileURL:fileURL];
    if (!mt) {
        iTLink = nil;
        return NO;
    }

    // used to check hash here, but it was expensive
    // now just triangulate with track ID and stored iTunes file path


    DDLogDebug(@"Remove iTunes track with ID: %@", [mt itunesDatabaseID]);
    ITunesResult *itr = [iTLink removeTrackByID:mt.itunesDatabaseID andFile:mt.itunesURL.path];
    if (!itr) {
        [self handleITunesNoResponseError];
        return NO;
    }

    BOOL result = NO;
    ITunesStatus status   = [itr[@"status"] integerValue];
    ITunesResultCode code = [itr[@"code"]   integerValue];
    if (status == ITunesStatus_OK &&
        code   == ITunesResultCode_TrackRemoved) {
        // remove iTunes file
        [[NSFileManager defaultManager] trashItemAtURL:mt.itunesURL
                                      resultingItemURL:nil
                                                 error:nil];
        DDLogDebug(@"... successfully removed track %@ and file %@. forgetting file.",
                   mt.itunesDatabaseID, mt.itunesURL.path);
        // remove managed track
        [_trackStore removeTrackAtURL:fileURL];
        result = YES;
    } else if (code == ITunesResultCode_TrackIDDoesNotExist) {
        DDLogDebug(@"... failed. Track %@ no longer exists. Forgetting file.",
                   mt.itunesDatabaseID);
        // remove managed track
        [_trackStore removeTrackAtURL:fileURL];
    } else if (code == ITunesResultCode_FileDoesNotExist) {
        DDLogDebug(@"... failed. File %@ no longer exists. Forgetting file.",
                   mt.itunesURL.path);
        // remove managed track
        [_trackStore removeTrackAtURL:fileURL];
    } else if (code == ITunesResultCode_TrackDoesNotMatch) {
        DDLogDebug(@"... failed. Track %@ and file %@ no longer match. Forgetting file.",
                   mt.itunesDatabaseID, mt.itunesURL.path);
        // remove managed track
        [_trackStore removeTrackAtURL:fileURL];
    } else {
        DDLogDebug(@"... failed. (%@) %@", itr[@"code"], itr[@"message"]);
    }
    iTLink = nil;
    return result;
}

- (void)matchExistingTrackToFile:(NSURL *)fileURL
                 iTunesLocations:(NSArray *)iTunesLocations
                     iTunesDbIDs:(NSArray *)iTunesDbIDs {
    NSNumber *isDir = nil;
    [fileURL getResourceValue:&isDir forKey:NSURLIsDirectoryKey error:nil];
    if (isDir.boolValue) return;
    
    // check if exact file is already in the database
    NSUInteger idx = [iTunesLocations indexOfObject:fileURL];
    if (idx != NSNotFound) { // found a prexisting track
        [_trackStore addTrackWithURL:fileURL
                             andDbID:iTunesDbIDs[idx]
                        andITunesURL:iTunesLocations[idx]];
        return;
    }

    // no exact match, have to search for possible matches
    [iTunesLocations enumerateObjectsUsingBlock:
     ^(NSURL *itURL, NSUInteger i, BOOL *stop) {
         if (![itURL isMemberOfClass:[NSNull class]]) { // check iTunes dead track
             *stop = [_trackStore addTrackWithURL:fileURL
                                          andDbID:iTunesDbIDs[i]
                                     andITunesURL:iTunesLocations[i]
                                   checkFileMatch:YES];
         }
     }];
}

- (BOOL) matchExistingTrackToFile:(NSURL *)fileURL
                           tracks:(NSArray *)tracks {
    @autoreleasepool {
        NSNumber *isDir = nil;
        [fileURL getResourceValue:&isDir forKey:NSURLIsDirectoryKey error:nil];
        if (isDir.boolValue) return NO;  // url is a directory, invalid!

        NSDictionary *d = @{ @"location" : fileURL };

        NSUInteger idxFirst = [tracks indexOfObject:d
                                      inSortedRange:(NSRange){0, tracks.count}
                                            options:NSBinarySearchingFirstEqual
                                    usingComparator:cmpStubs];

        if (idxFirst == NSNotFound) return NO;

        NSUInteger idxLast  = [tracks indexOfObject:d
                                      inSortedRange:(NSRange){0, tracks.count}
                                            options:NSBinarySearchingLastEqual
                                    usingComparator:cmpStubs];

        for (NSUInteger i = idxFirst; i <= idxLast; i++) {
            NSDictionary *track = tracks[i];
            if ([_trackStore addTrackWithURL:fileURL
                                     andDbID:track[@"dbID"]
                                andITunesURL:track[@"location"]
                              checkFileMatch:YES]) {
                return YES;
            }
        }
    }

    return NO;
}

- (void) retrieveiTunesState {
    @autoreleasepool {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Matching existing iTunes tracks..." }];

    NSFileManager *fm = [NSFileManager defaultManager];
    NSDirectoryEnumerator *dirContents = [fm enumeratorAtURL:_rootURL
                                  includingPropertiesForKeys:@[NSURLFileSizeKey, NSURLIsDirectoryKey]
                                                     options:NSDirectoryEnumerationSkipsHiddenFiles
                                                errorHandler:nil];

    DDLogInfo(@"Matching existing iTunes tracks with folder files...");

    // getAllFileTrackInfo returns separate arrays because it is fastest.
    id iTLink = [NSClassFromString(@"ITunesEventLink") new];
    ITunesResult *itr = [iTLink getAllFileTrackInfo];
    if (!itr) {
        [self handleITunesNoResponseError];
        return;
    }

    ITunesStatus status = [itr[@"status"] integerValue];
    if (status == ITunesStatus_OK) {
        NSArray *tracks = [self sortedITunesTracksByFilename:itr[@"value"]];

        // try to match existing iTunes tracks with files
        for (NSURL *fileURL in dirContents) {
            [self matchExistingTrackToFile:fileURL
                                    tracks:tracks];
        }
    } else if (status == ITunesStatus_AppleScriptError) {
        DDLogError(@"Could not complete iTunes operation: (%@) %@", itr[@"code"], itr[@"message"]);
        [self handleITunesNoResponseError];
        iTLink = nil;
        return;
    }

    iTLink = nil; // releasing class object seems to reduce memory leaks

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStopProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Done matching existing iTunes tracks" }];
    }
}

- (NSArray<NSDictionary *> *) sortedITunesTracksByFilename:(NSDictionary *)trackValues {
    @autoreleasepool {
    NSArray *dbIDs = trackValues[@"dbID"];
    NSArray *locations = trackValues[@"location"];

    NSMutableArray *res = [NSMutableArray array];
    for (NSInteger i = 0; i < dbIDs.count; i++) {
        // skip dead tracks
        if ([locations[i] isMemberOfClass:[NSNull class]]) continue;

        NSDictionary *d = @{ @"dbID": dbIDs[i], @"location": locations[i] };
        NSUInteger idx = [res indexOfObject:d
                              inSortedRange:(NSRange){0, res.count}
                                    options:NSBinarySearchingInsertionIndex
                            usingComparator:cmpLastPaths];
        [res insertObject:d atIndex:idx];
    }
    return [res copy];
    }
}

- (BOOL) isManageableFileType:(NSURL *)fileURL {
    CFStringRef fileExt = (__bridge CFStringRef) fileURL.pathExtension;
    CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,
                                                                fileExt, NULL);
    BOOL result = NO;
    if (fileUTI) {
        if (UTTypeConformsTo(fileUTI, kUTTypeAudio) &&
            (UTTypeConformsTo(fileUTI, kUTTypeWaveformAudio) ||
             UTTypeConformsTo(fileUTI, kUTTypeMP3)           ||
             UTTypeConformsTo(fileUTI, kUTTypeMPEG4Audio)    ||
             UTTypeConformsTo(fileUTI, CFSTR("public.aac-audio")) ||
             UTTypeConformsTo(fileUTI, kUTTypeAppleProtectedMPEG4Audio) ||
             UTTypeConformsTo(fileUTI, CFSTR("public.aifc-audio"))      ||
             UTTypeConformsTo(fileUTI, CFSTR("public.aiff-audio")))) {
                result = YES;
            }
        CFRelease(fileUTI);
    }
    return result;
}

- (BOOL) hasCargoDownloadProgressFile:(NSURL*)fileURL {
    NSURL *aspxFileURL = [fileURL URLByAppendingPathExtension:@"aspx"];
    return [[NSFileManager defaultManager] fileExistsAtPath:aspxFileURL.path];
}

- (BOOL) isCargoDownloadProgressFile:(NSURL*)fileURL {
    return [fileURL.pathExtension isEqualToString:@"aspx"];
}

- (void) handleITunesNoResponseError {
    DDLogError(@"No iTunes response. Check iTunes and restart.");
    [self stop];
    [[NSNotificationCenter defaultCenter] postNotificationName:kYPSPlaylistSyncStopNotification
                                                        object:self
                                                      userInfo:@{ @"message" : @"No iTunes response. Check iTunes and restart."}];
}

#pragma mark - YPSDirectoryEvents delegate
- (void) handleEvent:(CDEvent *)event {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStartProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Change detected..." }];

    // naively scan full directory for changes on every event
    // TODO: get smarter here if this is too slow
    YPSDirectorySnapshot *newSnap = [[YPSDirectorySnapshot alloc] initWithURL:_rootURL];
    YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc]
                                   initWithSnapshot:_lastSnapshot
                                   andOther:newSnap
                                   checkDirs:[NSSet setWithObject:event.URL]
                                   delegate:self];
    [cmp handleDirectoryChanges];
    _lastSnapshot = nil;
    _lastSnapshot = newSnap;

    [[NSNotificationCenter defaultCenter]
     postNotificationName:kYPSStopProgressOperationNotification
     object:self
     userInfo:@{ @"message" : @"Change evaluated..." }];
}

#pragma mark - YPSDirectoryComparator delegate
// "left-only" files have been removed
- (void) handleLeftFileOnly:(NSURL *)fileURL {
    DDLogInfo(@"File deletion detected: %@", fileURL);
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:kYPSTrackDeletionsKey])
        [self removeFile:fileURL];
}

// "right-only" files have been added
- (void) handleRightFileOnly:(NSURL *)fileURL {
    DDLogInfo(@"File addition detected: %@", fileURL);
    if ([self isCargoDownloadProgressFile:fileURL] ||
        [self hasCargoDownloadProgressFile:fileURL]) {
        DDLogInfo(@"... cargo download progress file detected. Skip this file.");
        return;
    }
    [self addFile:fileURL];
}

- (void) handleFileChanged:(NSURL *)fileURL {
    DDLogInfo(@"File change detected: %@", fileURL);
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([self isCargoDownloadProgressFile:fileURL] ||
        [self hasCargoDownloadProgressFile:fileURL]) {
        DDLogInfo(@"... cargo download progress file detected. Skip this file.");
    } else if ([ud boolForKey:kYPSTrackChangesKey]) {
        // just re-process changed files
        [self removeChangedFile:fileURL];
        [self addFile:fileURL];
    } else {
        DDLogInfo(@"... but ignore file changes");
    }
}

- (void) handleAllFilesHandled {
    [self syncWithiTunes];
}

@end
