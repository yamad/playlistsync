//
//  YPSPlaylistNameProvider.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/28/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSPlaylistNameProvider.h"

#import "constants.h"
#import "logging.h"
#import "xls.h"

@interface YPSPlaylistNameProvider ()
@property (nonatomic, copy, readwrite) NSString *defaultName;
@property (nonatomic, readwrite) NSMutableDictionary<NSString*, NSString*> *playlists;
@end


@implementation YPSPlaylistNameProvider
- (instancetype) init {
    if ((self = [super init])) {
        _playlists = [NSMutableDictionary dictionary];

        // use catch all name from default preferences, if none found
        if (!_defaultName)
            _defaultName = [[NSUserDefaults standardUserDefaults]
                            stringForKey:kYPSDefaultPlaylistNameKey];
    }
    return self;
}

- (NSString *) getNameForTag:(NSString *)tag {
    if ([tag isEqual:kYPSDefaultPlaylistTag])
        return _defaultName;
    return _playlists[tag];
}

- (void) addDiscoveredTag:(NSString *)tag {
    if (!_playlists[tag])
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kYPSNewTagDiscoveredNotification
         object:self
         userInfo:@{ @"tag" : tag }];
    _playlists[tag] = @"";
}

- (void) addName:(NSString *)name ForTag:(NSString *)tag {
    _playlists[tag] = name;
}

- (void) removeForTag:(NSString *)tag {
    if ([_playlists objectForKey:tag])
        [_playlists removeObjectForKey:tag];
}

- (void) stop {
}

@end


@interface YPSInternalNameProvider ()
@property (nonatomic, readwrite) NSMutableDictionary<NSString*, NSString*> *playlists;
@end

@implementation YPSInternalNameProvider

@dynamic playlists;

- (instancetype) init {
    self = [super init];
    if (!self) return nil;

    [self loadFromPlist];
    return self;
}

- (void) loadFromPlist {
    // load default names/tags from stored plist
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"playlistNames" ofType:@"plist"];
    NSData *plistData = [NSData dataWithContentsOfFile:path];
    NSError *error;
    NSPropertyListFormat format;

    // immutability flags weren't working for some reason, so just do the dumb thing and copy
    NSMutableDictionary *plist;
    plist = [NSPropertyListSerialization propertyListWithData:plistData
                                                      options:NSPropertyListImmutable
                                                       format:&format
                                                        error:&error];
    [plist enumerateKeysAndObjectsUsingBlock:^(NSString *tag, NSString *name, BOOL *stop) {
        self.playlists[tag] = name;
    }];
}

- (void) saveToPlist {
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"playlistNames" ofType:@"plist"];
    NSData *xmlData;
    NSError *error;

    xmlData = [NSPropertyListSerialization dataWithPropertyList:self.playlists
                                                         format:NSPropertyListXMLFormat_v1_0
                                                        options:0
                                                          error:&error];
    if(!xmlData) {
        DDLogDebug(@"Could not save playlist names");
    } else {
        [xmlData writeToFile:path atomically:YES];
    }
}

- (void) stop {
    [self saveToPlist];
}

@end

@implementation YPSExcelNameProvider {
    // make life easier while processing excel file
    xlsWorkBook  *pWB;
    xlsWorkSheet *pWS;
}

- (instancetype) initWithRootURL:(NSURL *)rootURL {
    self = [super init];
    if (!self)
        return nil;

    // try loading from excel spreadsheet
    if (![self loadFromXLS:[self getXLSFilePath:rootURL]])
        return nil;

    return self;
}

- (NSString *) getXLSFilePath:(NSURL *) rootURL {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSFileManager *fm = [NSFileManager defaultManager];

    NSString *xlsFileName = [ud stringForKey:kYPSPlaylistNameExcelFileKey];
    if (xlsFileName) {
        [fm changeCurrentDirectoryPath:[rootURL path]];
        NSString *fullPath = [xlsFileName stringByStandardizingPath];
        return fullPath;
    }
    return @"";
}

// read from excel sheet. returns YES if successful, NO if some error
- (BOOL) loadFromXLS:(NSString *)xlsFileName {
    //    char *fname = "/Users/jyh/src/playlistsync/Studio_Spreadsheet_01.xls";
    const char *fname = [xlsFileName UTF8String];

    struct st_row_data* row;

    // TODO: be smarter about detecting encoding
    pWB = xls_open(fname, "UTF-8");
    if (pWB == NULL)           return NO; // file can't be opened
    if (pWB->sheets.count < 1) return NO; // no information in first sheet

    
    pWS = xls_getWorkSheet(pWB, 0);
    xls_parseWorkSheet(pWS);

    // skip first row. assumes a header row
    for (WORD irow = 1; irow <= pWS->rows.lastrow; irow++) {
        row = xls_row(pWS, irow);
        [self parseExcelRow:row];
    }

    xls_close_WS(pWS);
    xls_close(pWB);

    return YES;
}

// load information from a row. fills internal stores from cell data
//
// the first string found on a row by itself is the default playlist name
// 'playlist rows' are rows with two strings,
//    the first string is the playlist name,
//    the second string is the playlist tag with preceding #
//    (e.g. Philip    #PB    --->   name: Philip, tag: PB)
- (NSString *) parseExcelRow:(struct st_row_data *) row {
    NSString *name;
    NSString *tag;
    BOOL foundName = NO;

    // pick out strings from the row
    for (WORD icol = 0; icol <= pWS->rows.lastcol; icol++) {

        // load cell
        xlsCell *cell = xls_cell(pWS, row->index, icol);
        if ((!cell) || (cell->isHidden))
            continue;

        // found a string
        if (cell->str != NULL && strlen((const char *)cell->str) > 0) {
            if (!foundName) {
                // name is the first string in the row
                name = [NSString stringWithUTF8String:(const char *)cell->str];
                foundName = YES;
            } else {
                // tag is the second string in the row
                // strip the first character. NOTE: sensitive to encoding
                tag = [NSString stringWithUTF8String:(const char *)((cell->str)+1)];
                break;
            }
        }
    }

    // found a unique tag and associated name
    if (name && tag && !self.playlists[tag]) {
        [self addName:name ForTag:tag];
        return tag;
    }

    // found a string by itself on a row. the first one is the default name
    if (!self.defaultName && name) {
        self.defaultName = name;
        return name;
    }

    return nil; // nothing found in row
}

@end