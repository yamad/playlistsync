//
//  YPSPlaylistTableViewModel.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/10/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSPlaylistTableViewModel.h"

#import "YPSPlaylistViewModel.h"
#import "YPSPlaylistNameProvider.h"

#import "constants.h"

@interface YPSPlaylistTableViewModel ()
@property (readwrite) NSMutableArray<YPSPlaylistViewModel *> *playlists;
@property NSMutableArray *observers;
@end

@implementation YPSPlaylistTableViewModel {
    NSRegularExpression *_tagRegex;
}

- (instancetype) init {
    self = [super init];
    if (!self) return nil;

    _playlists = [NSMutableArray array];
    _tagRegex = [NSRegularExpression
                 regularExpressionWithPattern: @"^[A-Z][A-Z0-9]$"
                 options:0
                 error:nil];
    _observers = [NSMutableArray array];
    return self;
}

- (void) dealloc {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    for (id o in _observers) {
        [nc removeObserver:o];
    }
    [_observers removeAllObjects];
}

- (void) setNameProvider:(YPSPlaylistNameProvider *)nameProvider {
    _nameProvider = nameProvider;

    id o = [[NSNotificationCenter defaultCenter]
            addObserverForName:kYPSNewTagDiscoveredNotification
            object:_nameProvider
            queue:[NSOperationQueue mainQueue]
            usingBlock:^(NSNotification *note) {
                NSString *tag = note.userInfo[@"tag"];

                // add the new tag
                YPSPlaylistViewModel *pvm;
                pvm = [YPSPlaylistViewModel
                       playlistFromProviderWithTag:tag andName:@""];
                [_playlists addObject:pvm];

                // forward notification from self
                [[NSNotificationCenter defaultCenter] postNotificationName:kYPSNewTagDiscoveredNotification object:self];
                   }];
    [_observers addObject:o];

    for (NSString* key in _nameProvider.playlists) {
        NSString *name = _nameProvider.playlists[key];
        YPSPlaylistViewModel *pvm = [[YPSPlaylistViewModel alloc] init];
        pvm = [YPSPlaylistViewModel playlistFromProviderWithTag:key
                                                        andName:name];
        [self insertPlaylistSortedOrder:pvm];
    }
}

- (void) syncToModel {
    for (YPSPlaylistViewModel *pvm in _playlists) {
        if (pvm.isValid && (pvm.isNew || pvm.isDirty)) {
            [_nameProvider addName:pvm.name ForTag:pvm.tag];
            if (pvm.isNew)   pvm.isNew   = NO;
            if (pvm.isDirty) pvm.isDirty = NO;
        }
    }
}

- (NSInteger) addNewPlaylist {
    YPSPlaylistViewModel *pvm = [[YPSPlaylistViewModel alloc] init];
    pvm.isNew = YES;
    pvm.isDirty = NO;
    [_playlists addObject:pvm];
    return _playlists.count - 1;
}

- (NSInteger) insertPlaylistSortedOrder:(YPSPlaylistViewModel *)pvm {
    NSUInteger newIndex = [_playlists indexOfObject:pvm
                                      inSortedRange:(NSRange){0, _playlists.count}
                                            options:NSBinarySearchingInsertionIndex
                                    usingComparator:^NSComparisonResult(YPSPlaylistViewModel *pvm1, YPSPlaylistViewModel *pvm2) {
                                        return [pvm1.tag compare:pvm2.tag];
                                    }];
    [_playlists insertObject:pvm atIndex:newIndex];
    return newIndex;
}

- (BOOL) removePlaylistAtIndex:(NSInteger)index {
    if (index >= _playlists.count) return NO;

    [_nameProvider removeForTag:_playlists[index].tag];
    [_playlists removeObjectAtIndex:index];
    return YES;
}

- (BOOL) validateTag:(NSString *)newTag {
    // new tag must match tag pattern
    if ([_tagRegex numberOfMatchesInString:newTag
                                  options:0
                                    range:NSMakeRange(0, newTag.length)] == 0) {
        return NO;
    }

    // new tag must not be a duplicate
    __block BOOL isDuplicate = NO;
    [_playlists enumerateObjectsUsingBlock:
     ^(YPSPlaylistViewModel *pvm, NSUInteger idx, BOOL *stop) {
         if ([pvm.tag isEqual:newTag]) {
             isDuplicate = YES;
             *stop = YES;
         }
     }];

    if (isDuplicate) return NO;
    return YES;
}

@end
