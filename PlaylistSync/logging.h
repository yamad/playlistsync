//
//  logging.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/26/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#ifndef logging_h
#define logging_h

#define LOG_LEVEL_DEF ddLogLevel
#import <CocoaLumberjack/CocoaLumberjack.h>

#ifdef DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
//static const DDLogLevel ddLogLevel = DDLogLevelError;
#else
static const DDLogLevel ddLogLevel = DDLogLevelInfo;
#endif

#endif /* logging_h */
