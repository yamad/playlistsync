//
//  YPSPlaylistManager.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/21/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;
@class YPSPlaylistNameProvider;

#import "YPSPlaylist.h"

@interface YPSPlaylistManager : NSObject
@property NSMutableDictionary<NSString *, YPSPlaylist *> *playlistsByTag;
@property (weak) YPSPlaylistNameProvider *nameProvider;

// don't need per playlist deletion tracking
// removing files from the main library removes from all playlists
@property NSMutableSet<NSURL *> *filesToRemove;
@property NSMutableSet<NSURL *> *filesChanged;


- (instancetype) initWithNameProvider:(YPSPlaylistNameProvider *) np;
- (BOOL) addFileAtURL:(NSURL *)fileURL error:(NSError **)error;
- (BOOL) removeFileAtURL:(NSURL *)fileURL error:(NSError **)error;
- (BOOL) removeChangedFileAtURL:(NSURL *)fileURL error:(NSError **)error;
- (YPSPlaylist *) addPlaylistWithTag:(NSString*) tag;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *display;
@end
