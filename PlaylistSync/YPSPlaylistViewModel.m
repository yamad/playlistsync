//
//  YPSPlaylistViewModel.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/11/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSPlaylistViewModel.h"

@implementation YPSPlaylistViewModel

@synthesize tag  = _tag;
@synthesize name = _name;

- (instancetype) init {
    self = [super init];
    if (!self) return nil;

    _tag  = @"";
    _name = @"";
    _isValid = NO;
    _isNew   = YES;
    _isDirty = NO;
    return self;
}

+ (YPSPlaylistViewModel *) playlistFromProviderWithTag:(NSString *)tag
                                               andName:(NSString*)name {
    YPSPlaylistViewModel *pvm = [[YPSPlaylistViewModel alloc] init];
    pvm.name    = name;
    pvm.tag     = tag;
    pvm.isNew   = NO;
    pvm.isDirty = NO;
    pvm.isValid = YES;
    return pvm;
}

- (void) setTag:(NSString *)tag {
    if (![tag isEqual:_tag]) {
        _isDirty = YES;
    }
    _tag = tag;
}
- (NSString *) tag {
    return _tag;
}

- (void) setName:(NSString *)name {
    if (![name isEqual:_name]) {
        _isDirty = YES;
    }
    _name = name;
}
- (NSString *) name {
    return _name;
}

@end