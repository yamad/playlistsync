//
//  YPSDirectoryEvents.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/20/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

// TODO: design
//   monitors directory and dispatches a DirectoryEvent, which puts itself on a GCD (ala MWDirectoryWatcher) and waits until the directory has stopped changing, then notifies (on Notification center?) that a DirectoryEvent has occurred.

#import "YPSDirectoryEvents.h"

#import "CDEvents.h"
#import "CDEventsDelegate.h"

#import "logging.h"

static NSTimeInterval const kYPSNotificationLatency = 2.0;
static const char *kYPSDirectoryEventsGCDQueue = "kYPSDirectoryEventsGCDQueue";
NSString *const kYPSDirectoryEventNotification;

@interface YPSDirectoryEvents () <CDEventsDelegate>
@property (nonatomic, strong) CDEvents *eventStream;
@property (nonatomic, weak) id <YPSDirectoryEventsDelegate> delegate;
@property (nonatomic) dispatch_queue_t queue;
@property CDEventIdentifier lastEventID;
@property CDEvent *lastEvent;

- (void) createEventStream;
- (void) checkEventURLDoneChanging:(CDEvent *)event
                      dispatchTime:(int64_t)delay
                        checksLeft:(int)count;
@end


@implementation YPSDirectoryEvents
- (instancetype)initWithURL:(NSURL *)watchedURL
                   delegate:(id<YPSDirectoryEventsDelegate>)delegate
           startImmediately:(BOOL)startImmediately
{
    if ((self = [super init])) {
        _watchedURL  = [watchedURL copy];
        _lastEventByPath = [NSMutableDictionary dictionary];
        _lastDirSnapByPath = [NSMutableDictionary dictionary];
        _queue = dispatch_queue_create(kYPSDirectoryEventsGCDQueue, 0);
        _lastEventID = kCDEventsSinceEventNow;
        _delegate = delegate;
        if (startImmediately)
            [self start];
    }
    return self;
}

- (instancetype)initWithURL:(NSURL *)watchedURL
                   delegate:(id<YPSDirectoryEventsDelegate>)delegate {
    return [self initWithURL:watchedURL delegate:delegate startImmediately:YES];
}

- (instancetype) initWithURL:(NSURL *)watchedURL {
    return [self initWithURL:watchedURL delegate:nil];
}

- (void) dealloc {
    _delegate = nil;
}

- (void) start {
    [self createEventStream];
}

- (void) stop {
    _lastEventID = _eventStream.lastEvent.identifier;
    _eventStream = nil;
}

- (void) restart {
    [self stop];
    [self start];
}

- (void) createEventStream {
    CDEventsEventStreamCreationFlags creationFlags = kCDEventsDefaultEventStreamFlags;
    __weak id weakSelf = self;
    _eventStream = [[CDEvents alloc] initWithURLs:@[_watchedURL]
                                         delegate:weakSelf
                                        onRunLoop:[NSRunLoop currentRunLoop]
                             sinceEventIdentifier:_lastEventID
                              notificationLatency:kYPSNotificationLatency
                          ignoreEventsFromSubDirs:NO
                                      excludeURLs:nil
                              streamCreationFlags:creationFlags];
}

- (void)URLWatcher:(CDEvents *)URLWatcher eventOccurred:(CDEvent *)event {
    DDLogDebug(@"Event occurred: %@", event);

    // check and store latest event
    if (!_lastEvent ||
        [event.date laterDate:_lastEvent.date] == event.date) {
        _lastEvent = event;
    }

    // check and store latest event for given URL
    if (!_lastEventByPath[event.URL] ||
        [event.date laterDate:_lastEventByPath[event.URL].date] == event.date) {
        _lastEventByPath[event.URL] = event;
        _lastDirSnapByPath[event.URL] = [[YPSDirectorySnapshot alloc] initWithURL:event.URL
                                                                      shallowOnly:YES];
    }

    // wait for URL to stabilize
    int64_t delay = kYPSNotificationLatency * 8 * NSEC_PER_SEC;
    [self checkEventURLDoneChanging:event
                       dispatchTime:delay
                         checksLeft:5];
}

- (void) checkEventURLDoneChanging:(CDEvent *)event
                      dispatchTime:(int64_t)delay
                        checksLeft:(int)count
{
    DDLogDebug(@"Checking if event is done changing (%d left)...", count);
    if (_lastEvent &&
        [event.date laterDate:_lastEvent.date] != event.date) {
        DDLogDebug(@"Event ignored (later event already seen): %@", event);
        return;
    }

    YPSDirectorySnapshot *lastSnap = _lastDirSnapByPath[event.URL];
    if (lastSnap) {
        DDLogDebug(@"Examining directory for changes: %@", event.URL);
        YPSDirectorySnapshot *curSnap = [[YPSDirectorySnapshot alloc] initWithURL:event.URL
                                                                      shallowOnly:YES];
        YPSDirectoryComparator *cmp = [[YPSDirectoryComparator alloc]
                                       initWithSnapshot:lastSnap
                                               andOther:curSnap
                                            shallowOnly:YES];
        if (cmp.hasChanges) {
            DDLogDebug(@"... directory changes discovered");
            _lastDirSnapByPath[event.URL] = curSnap;
            count = 5; // reset checks before deciding URL is stablized
        } else {
            DDLogDebug(@"... directory has no changes");
            count -= 1;
        }
    }

    if (count == 0) {
        _lastEventByPath[event.URL] = event;
        return [self handleEventDoneChanging:event];
    }

    // wait for URL to stabilize
    // provisionally issue event notification after 5x notification periodsC
    __weak id weakSelf = self;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delay);
    dispatch_after(popTime, self.queue, ^(void) {
        [weakSelf checkEventURLDoneChanging:event
                               dispatchTime:delay
                                 checksLeft:count];
    });
}

// check if event already exists that is more recent
    // dispatch a notification after some latency
    //    notification runs if no FSEvents have issued in some multiple of notificationLatencies
- (void) handleEventDoneChanging:(CDEvent *)event {
    DDLogDebug(@"Event path done changing: %@", event);
    __weak id weakSelf = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:kYPSDirectoryEventNotification
                                                        object:weakSelf
                                                      userInfo:@{@"event" : event }];

    if (_delegate) {
        [_delegate handleEvent:event];
    }
}

@end
