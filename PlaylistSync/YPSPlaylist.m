//
//  YPSPlaylist.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/22/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSPlaylist.h"

@implementation YPSPlaylist
- (instancetype) initWithTag:(NSString*)tag andName:(NSString*)name andFiles:(NSSet<NSURL *> *)fileURLs {
    if ((self = [super init])) {
        _tag = tag;
        _name = name;
        _filesToAdd = [NSMutableSet set];
    }
    return self;
}

- (instancetype) initWithTag:(NSString*)tag andFile:(NSURL *)fileURL {
    return [self initWithTag:tag
                     andName:nil
                    andFiles:[NSSet setWithObject:fileURL]];
}

- (instancetype) initWithTag:(NSString*)tag {
    return [self initWithTag:tag andName:nil andFiles:nil];
}

- (instancetype) initWithTag:(NSString *)tag andName:(NSString *)name {
    return [self initWithTag:tag andName:name andFiles:nil];
}

- (BOOL) addFileAtURL:(NSURL *)fileURL {
    // NSSet semantics discard duplicates automatically
    [_filesToAdd addObject:fileURL];
    return YES;
}

- (BOOL) rename:(NSString *)newName {
    if (newName) {
        if (newName.length == 0) // if setting name to nothing, use tag as name for iTunes
            _renameName = _tag;
        else if (newName != _name) // ignore "change" to pre-existing name
            _renameName = newName;
        return YES;
    }
    return NO;
}

@synthesize name = _name;
- (void) setName:(NSString *)n {
    _name = n;
}

- (NSString *) name {
    if (!_name)            return _tag;
    if (_name.length == 0) return _tag;
    return _name;
}

- (NSString *) display {
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[NSString stringWithFormat:@"%@ / %@", _tag, _name]];

    [result addObject:@"\nFiles to add:"];
    [_filesToAdd enumerateObjectsUsingBlock:^(NSURL *url, BOOL *stop) {
        [result addObject:[NSString stringWithFormat:@"  %@", url.path]];
    }];

    [result addObject:@"\n\n"];
    return [result componentsJoinedByString:@"\n"];
}

@end

