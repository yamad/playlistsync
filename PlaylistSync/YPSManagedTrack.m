//
//  YPSManagedTrack.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/24/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSManagedTrack.h"

@implementation YPSManagedTrack
- (instancetype) initWithURL:(NSURL*)fileURL
                 andFileSize:(NSNumber*)fsize
                     andDbID:(NSNumber*)dbID
                andITunesURL:(NSURL*)itunesURL
                     andDate:(NSDate*)addDate {

    self = [super init];
    if (self) {
        _fileURL = [fileURL copy];
        _trackDate = [addDate copy];
        _fileSize = [fsize copy];
        _itunesURL = [itunesURL copy];
        _itunesDatabaseID = [dbID copy];
    }
    return self;
}

- (instancetype) initWithURL:(NSURL*)url
                     andDbID:(NSNumber*)dbID
                andITunesURL:(NSURL*)itunesURL {
    NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:url.path error:nil];
    return [self initWithURL:url andFileSize:[NSNumber numberWithLongLong:[attrs fileSize]]
                     andDbID:dbID andITunesURL:itunesURL
                     andDate:[NSDate date]];
}

- (NSString *) description {
    return [NSString stringWithFormat:@"<%@: %p, %@ %@ %@ >",
                    [self class], self, _fileURL.lastPathComponent,
                    _itunesURL.lastPathComponent, _itunesDatabaseID];
}

@end