//
//  YPSPlaylistManager.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/21/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSPlaylistManager.h"

#import "YPSPlaylistNameProvider.h"

#import "constants.h"
#import "logging.h"

@implementation YPSPlaylistManager {
    NSRegularExpression *_plTagRegex;
}

- (instancetype) init {
    YPSPlaylistNameProvider *np = [[YPSPlaylistNameProvider alloc] init];
    return [self initWithNameProvider:np];
}

- (instancetype) initWithNameProvider:(YPSPlaylistNameProvider *) np {
    if ((self = [super init])) {
        // get names from preferences/files
        _nameProvider = np;

        // default playlist is required
        YPSPlaylist *defaultPL = [[YPSPlaylist alloc] initWithTag:kYPSDefaultPlaylistTag
                                                          andName:_nameProvider.defaultName];
        if (!defaultPL)  return nil;

        // setup internal variables
        _filesToRemove  = [NSMutableSet set];
        _filesChanged   = [NSMutableSet set];
        _playlistsByTag = [NSMutableDictionary dictionaryWithObject:defaultPL
                                                             forKey:kYPSDefaultPlaylistTag];
        _plTagRegex = [NSRegularExpression
                       regularExpressionWithPattern:kYPSTagRegex
                       options:0
                       error:nil];
    }
    return self;
}

- (BOOL) addFileAtURL:(NSURL *)fileURL error:(NSError **)error {
    NSString *tag = [self findLastPlaylistTagInString:fileURL.path];
    if (tag)
        [self addFileAtURL:fileURL withTag:tag];
    // add everything to default playlist
    [self addFileAtURL:fileURL withTag:kYPSDefaultPlaylistTag];
    return YES;
}

- (BOOL) removeFileAtURL:(NSURL *)url
                   error:(NSError **)error {
    [_filesToRemove addObject:url];
    return NO;
}

- (BOOL) removeChangedFileAtURL:(NSURL *)url
                          error:(NSError **)error {
    [_filesChanged addObject:url];
    return NO;
}

- (YPSPlaylist *) addFileAtURL:(NSURL *)fileURL withTag:(NSString *)tag {
    // retrieve or create suitable playlist
    YPSPlaylist *pl = _playlistsByTag[tag];
    if (!pl)
        pl = [self addPlaylistWithTag:tag];

    // add file to playlist, duplicates are handled automatically.
    [pl addFileAtURL:fileURL];
    return pl;
}

- (YPSPlaylist *) addPlaylistWithTag:(NSString*) tag {
    YPSPlaylist *pl      = [[YPSPlaylist alloc] initWithTag:tag];
    pl.manager = self;

    NSString *storedName = [_nameProvider getNameForTag:tag];
    if (storedName)
        pl.name = storedName;
    _playlistsByTag[tag] = pl;

    if (!storedName)
        [_nameProvider addDiscoveredTag:tag];
    
    return pl;
}

- (NSString *) display {
    NSMutableArray<NSString *> *result = [NSMutableArray array];
    [result addObject:[NSString stringWithFormat:@"%lu playlists", _playlistsByTag.count]];
    [result addObject:@"------------------------------"];

    [_playlistsByTag enumerateKeysAndObjectsUsingBlock:^(NSString *tag, YPSPlaylist *pl, BOOL *stop) {
        [result addObject:[pl display]];
    }];

    return [result componentsJoinedByString:@"\n"];
}

// return the _last_ 'playlist tag' (#AA) in a file path string
- (NSString *) findLastPlaylistTagInString:(NSString *) fullPath {

    NSArray *tags = [_plTagRegex matchesInString:fullPath
                                        options:0
                                          range:NSMakeRange(0, fullPath.length)];

    if (tags.count > 0) {
        NSTextCheckingResult *match = tags.lastObject;
        return [fullPath substringWithRange:[match rangeAtIndex:1]];
    }
    return nil;  // didn't find a playlist tag
}
@end
