//
//  YPSPlaylistViewModel.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/11/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YPSPlaylistViewModel : NSObject
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *name;
@property BOOL isNew;
@property BOOL isDirty;
@property BOOL isValid;
+ (YPSPlaylistViewModel *) playlistFromProviderWithTag:(NSString *)tag
                                               andName:(NSString*)name;
@end