//
//  YPSFileComparator.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/8/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSFileComparator.h"

@implementation YPSiTunesFileMatcher
+ (BOOL) matchAtURL:(NSURL *)url1 andURL:(NSURL *)url2 error:(NSError *__autoreleasing *)error {
    NSFileManager *fm = [NSFileManager defaultManager];

    if (![fm fileExistsAtPath:url1.path isDirectory:NO])                 return NO;
    if (![fm fileExistsAtPath:url2.path isDirectory:NO])                 return NO;
    if (![YPSFilenameStubMatcher matchAtURL:url1 andURL:url2 error:nil]) return NO;
    if (![YPSFileSizeMatcher     matchAtURL:url1 andURL:url2 error:nil]) return NO;
    if (![YPSFileContentsMatcher matchAtURL:url1 andURL:url2 error:nil]) return NO;
    return YES;
}
@end

@implementation YPSFilenameStubMatcher
+ (BOOL) matchAtURL:(NSURL *)url1 andURL:(NSURL *)url2 error:(NSError *__autoreleasing *)error {
    NSString *url1Name = url1.lastPathComponent;
    NSString *url2Name = url2.lastPathComponent;

    // extensions must match
    NSString *url1Ext = url1.pathExtension;
    NSString *url2Ext = url2.pathExtension;
    if (![url1Ext isEqual:url2Ext]) return NO;

    NSCharacterSet *ws = [NSCharacterSet whitespaceCharacterSet];
    NSString *url1Stub = [url1Name.stringByDeletingPathExtension stringByTrimmingCharactersInSet:ws];
    NSString *url2Stub = [url2Name.stringByDeletingPathExtension stringByTrimmingCharactersInSet:ws];
    // test prefixes because copies may have suffix (e.g. "name" and "name 1")
    if ([url2Stub hasPrefix:url1Stub]) return YES;

    // no match
    return NO;
}
@end


@implementation YPSFileSizeMatcher
+ (BOOL) matchAtURL:(NSURL *)url1 andURL:(NSURL *)url2 error:(NSError *__autoreleasing *)error {
    NSNumber *fsize1;
    NSNumber *fsize2;

    [url1 getResourceValue:&fsize1 forKey:NSURLFileSizeKey error:nil];
    [url2 getResourceValue:&fsize2 forKey:NSURLFileSizeKey error:nil];

    if ([fsize1 isEqual:fsize2]) return YES;
    return NO;
}
@end


static BOOL filesEqual(const char *fname1, const char *fname2);

@implementation YPSFileContentsMatcher
// return YES if two files are equal byte-for-byte. otherwise, return NO
+ (BOOL) matchAtURL:(NSURL *)url1 andURL:(NSURL *)url2 error:(NSError *__autoreleasing *)error {
    return [YPSFileContentsMatcher filesEqualWithURL:url1 andURL:url2];
}

+ (BOOL) filesEqualWithURL:(NSURL*)url1 andURL:(NSURL*)url2 {
    return filesEqual(url1.path.UTF8String, url2.path.UTF8String);
}

// TODO: DOES NOT WORK!
+ (void) asyncMatchWithURL:(NSURL*)url1 andURL:(NSURL*)url2 completionBlock:(void (^)(BOOL))callback {
    dispatch_io_t d1, d2;
    dispatch_queue_t q;

    q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    d1 = dispatch_io_create_with_path(DISPATCH_IO_STREAM, url1.path.UTF8String, O_RDONLY, 0, q, ^(int error){});
    d2 = dispatch_io_create_with_path(DISPATCH_IO_STREAM, url2.path.UTF8String, O_RDONLY, 0, q, ^(int error){});

    dispatch_io_read(d1, 0, SIZE_MAX, q, ^(bool done1, dispatch_data_t data1, int err1) {
        __block BOOL isEqual = NO;
        if (err1 || !data1) {
            callback(isEqual);
            return;
        }

        if (done1) {
            callback(isEqual);
            return;
        }

        // iterate over memory blocks from file 1
        dispatch_data_apply(data1, ^bool(dispatch_data_t region, size_t offset, const void *buf1, size_t size1) {
            isEqual = YES;
            // read same size block from file 2
            dispatch_io_read(d2, 0, size1, q, ^(bool done2, dispatch_data_t data2, int err2) {
                if (err2 || !data2) {
                    isEqual = NO;
                    return;
                }

                const void *buf2;
                size_t size2;
                dispatch_data_t map;
                map = dispatch_data_create_map(data2, &buf2, &size2);

                if (size1 != size2) {
                    isEqual = NO;
                    return;
                }

                if (memcmp(buf1, buf2, size1) != 0) {
                    isEqual = NO;
                    return;
                }
            });
            return YES;
        });
    });
}

static const size_t BUFSIZE = 2046;
BOOL filesEqual(const char *fname1, const char *fname2) {
    FILE *f1 = fopen(fname1, "r");
    FILE *f2 = fopen(fname2, "r");

    BOOL isEqual = NO;
    if (f1 == NULL || f2 == NULL)
        return NO;    // not equal, something went wrong

    // assume files are equal sizes because we already checked

    char *buf1[BUFSIZE] = {0};
    char *buf2[BUFSIZE] = {0};
    size_t c1, c2;
    while (!feof(f1) && !feof(f2)) {
        c1 = fread(&buf1, 1, BUFSIZE, f1);
        c2 = fread(&buf2, 1, BUFSIZE, f2);
        if (c1 != c2)
            goto cleanup; // not equal, unequal buffer fill

        if (memcmp(buf1, buf2, c1) != 0)
            goto cleanup; // not equal, bytes aren't equal
    }
    isEqual = YES;        // equal! no differences found

    // make sure to tidy up. goto considered useful.
cleanup:
    fclose(f1);
    fclose(f2);
    return isEqual;
}

@end
