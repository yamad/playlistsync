//
//  ViewController.h
//  Shangri-La Playlists
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainViewController : NSViewController
@property (weak) IBOutlet NSPathCell *rootFolderSelector;
@property (weak) IBOutlet NSTextField *defaultPlaylistField;

@property (weak) IBOutlet NSButton *autoRestartSelect;
@property (weak) IBOutlet NSButton *startButton;
@property (weak) IBOutlet NSButton *stopButton;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;
@property (weak) IBOutlet NSTextField *statusMessage;
@property (weak) IBOutlet NSButton *trackDeletionsSelect;

- (IBAction)chooseRootFolder:(NSPathCell *)sender;
- (IBAction)updateDefaultPlaylistName:(NSTextField *)sender;
- (IBAction)selectStartButton:(NSButton *)sender;
- (IBAction)selectStopButton:(NSButton *)sender;
- (IBAction)toggleAutoRestart:(NSButton *)sender;
- (IBAction)toggleTrackFileDeletions:(NSButton *)sender;
@end
