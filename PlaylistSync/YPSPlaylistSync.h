//
//  YPSPlaylistSync.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/21/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

#import "YPSPlaylistManager.h"
#import "YPSManagedTrackStore.h"

@interface YPSPlaylistSync : NSObject
@property (readonly) NSURL *rootURL;
@property YPSPlaylistManager *playlistManager;
@property YPSManagedTrackStore *trackStore;
@property NSMutableSet<NSURL *> *problemFiles;
@property NSMutableSet<NSURL *> *ignoredFiles;
@property BOOL isRunning;

- (instancetype) initWithRootURL:(NSURL *)rootURL
                 backgroundQueue:(dispatch_queue_t)queue
                    nameProvider:(YPSPlaylistNameProvider *)nameProvider
                startImmediately:(BOOL)startImmediately NS_DESIGNATED_INITIALIZER;
- (instancetype) initWithRootURL:(NSURL *)rootURL
                startImmediately:(BOOL) startImmediately;
- (instancetype) initWithRootURL:(NSURL *)rootURL;

/*! @abstract Use initWithRootURL:... instead */
-(instancetype) init NS_UNAVAILABLE;

- (void) retrieveiTunesState;
- (BOOL) setupFullSync;
- (BOOL) syncWithiTunes;
- (BOOL) start;
- (BOOL) stop;
@end
