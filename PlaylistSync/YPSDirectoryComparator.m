//
//  YPSDirectoryComparator.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSDirectoryComparator.h"

#import "YPSDirectorySnapshot.h"
#import "logging.h"

NSString *const YPSFileAddedNotification   = @"YPSFileAddedNotification";
NSString *const YPSFileRemovedNotification = @"YPSFileRemovedNotification";
NSString *const YPSFileChangedNotification = @"YPSFileChangedNotification";

@interface YPSDirectoryComparator ()
    @property (nonatomic, weak) id <YPSDirectoryComparatorChangeDelegate> delegate;
@end

@implementation YPSDirectoryComparator {
    BOOL _leftIsOlder;
    BOOL _isRootDir;
}

- (instancetype) init
{
    @throw [NSException
            exceptionWithName:NSInternalInconsistencyException
            reason:@"Must use -initWithDir:..."
            userInfo:nil];
}

- (void) dealloc
{
    _delegate = nil; // release delegate if we have one
}

- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
{
    return [self initWithSnapshot:leftSnap andOther:rightSnap
                        checkDirs:[NSSet set] delegate:nil];
}

- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                      shallowOnly:(BOOL) shallowCheck
{
    return [self initWithSnapshot:leftSnap andOther:rightSnap
                        checkDirs:[NSSet set] delegate:nil
                      shallowOnly:YES isRoot:YES];
}

- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                        checkDirs:(NSSet<NSURL *> *) dirsToCheck
                         delegate:(id<YPSDirectoryComparatorChangeDelegate>) delegate
{
    return [self initWithSnapshot:leftSnap andOther:rightSnap
                        checkDirs:dirsToCheck delegate:delegate
                      shallowOnly:NO isRoot:YES];
}

- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                        checkDirs:(NSSet<NSURL *> *) dirsToCheck
                         delegate:(id<YPSDirectoryComparatorChangeDelegate>) delegate
                      shallowOnly:(BOOL) shallowCheck
                           isRoot:(BOOL) isRootDir
{
    @autoreleasepool {
        if ((self = [super init])) {
            _delegate = delegate;

            _hasShallowChanges = NO;
            _hasDeepChanges    = NO;
            _hasChanges        = NO;
            _isRootDir         = isRootDir;

            // compare files in the directory
            _filesInCommon = [leftSnap.files mutableCopy];
            [_filesInCommon intersectSet:rightSnap.files];

            _filesInLeftOnly = [leftSnap.files mutableCopy];
            [_filesInLeftOnly minusSet:rightSnap.files];

            _filesInRightOnly = [rightSnap.files mutableCopy];
            [_filesInRightOnly minusSet:leftSnap.files];

            // compare directories by name
            NSSet *leftDirs  = [NSSet setWithArray:leftSnap.subdirs.allKeys];
            NSSet *rightDirs = [NSSet setWithArray:rightSnap.subdirs.allKeys];

            _dirsInCommon = [leftDirs mutableCopy];
            [_dirsInCommon intersectSet:rightDirs];

            _dirsInLeftOnly = [leftDirs mutableCopy];
            [_dirsInLeftOnly minusSet:rightDirs];

            _dirsInRightOnly = [rightDirs mutableCopy];
            [_dirsInRightOnly minusSet:leftDirs];


            // get flat list of files in unmatched directories
            // need to capture these now because we don't hold the snapshots
            _filesInLeftOnlyDirs = [NSMutableSet set];
            for (NSURL *dir in _dirsInLeftOnly) {
                YPSDirectorySnapshot *dirSnap = leftSnap.subdirs[dir];
                [_filesInLeftOnlyDirs unionSet:dirSnap.allFiles];
            }

            _filesInRightOnlyDirs = [NSMutableSet set];
            for (NSURL *dir in _dirsInRightOnly) {
                YPSDirectorySnapshot *dirSnap = rightSnap.subdirs[dir];
                [_filesInRightOnlyDirs unionSet:dirSnap.allFiles];
            }


            // compare common files for changes
            _filesChanged = [NSMutableSet set];
            [_filesInCommon enumerateObjectsUsingBlock:^(NSURL *fileURL, BOOL *stop) {
                // we've already checked that both snapshots hold these files. don't check for existence.
                // generation IDs (by docs) change when file content changes
                id leftGenID = leftSnap.generationIDs[fileURL];
                id rightGenID = rightSnap.generationIDs[fileURL];
                if (![leftGenID isEqual:rightGenID]) {
                    [_filesChanged addObject:fileURL];
                    DDLogDebug(@"Change found: %@", fileURL);
                }
            }];

            if (_filesChanged.count     > 0 ||
                _filesInLeftOnly.count  > 0 ||
                _filesInRightOnly.count > 0 ||
                _dirsInLeftOnly.count   > 0 ||
                _dirsInRightOnly.count  > 0) {
                _hasShallowChanges = YES;
                _hasChanges = YES;
            }

            if (!shallowCheck) {
                // deep compare common subdirectories
                _dirCompares = [NSMutableDictionary dictionaryWithCapacity:_dirsInCommon.count];
                NSDictionary *leftDirSnaps = leftSnap.subdirs;
                NSDictionary *rightDirSnaps = rightSnap.subdirs;
                [_dirsInCommon enumerateObjectsUsingBlock:^(NSURL *dir, BOOL *stop) {
                    // generation IDs (by docs) change when subdirectory file content changes
                    id leftGenID = leftSnap.generationIDs[dir];
                    id rightGenID = rightSnap.generationIDs[dir];
                    if (![dirsToCheck containsObject:dir] &&
                        [leftGenID isEqual:rightGenID]) {
                        return; // subdirs are equal, don't need to check
                    }

                    YPSDirectoryComparator *dirCmp = [[YPSDirectoryComparator alloc]
                                                      initWithSnapshot:leftDirSnaps[dir]
                                                      andOther:rightDirSnaps[dir]
                                                      checkDirs:dirsToCheck
                                                      delegate:delegate
                                                      shallowOnly:NO
                                                      isRoot:NO];
                    _dirCompares[dir] = dirCmp;
                    if (dirCmp.hasChanges) {
                        _hasDeepChanges = YES;
                        _hasChanges = YES;
                    }
                }];
            }
            
            // mark with most recent timestamp of the snapshots
            _timestamp = [leftSnap.timestamp laterDate:rightSnap.timestamp];
            
            _leftIsOlder = YES;
            if ([leftSnap.timestamp isEqualToDate:_timestamp])
                _leftIsOlder = NO;
        }
        
        return self;
    }
}

- (NSSet<NSURL *> *) allFilesInLeftOnly {
    NSMutableSet<NSURL *> *acc = [NSMutableSet setWithSet:_filesInLeftOnly];
    [acc unionSet:_filesInLeftOnlyDirs];
    if (_hasDeepChanges) {
        [_dirCompares enumerateKeysAndObjectsUsingBlock:^(NSURL *dirURL, YPSDirectoryComparator *dirCmp, BOOL * stop) {
            [acc unionSet:dirCmp.allFilesInLeftOnly];
        }];
    }
    return [acc copy];
}

- (NSSet<NSURL *> *) allFilesInRightOnly {
    NSMutableSet<NSURL *> *acc = [NSMutableSet setWithSet:_filesInRightOnly];
    [acc unionSet:_filesInRightOnlyDirs];
    if (_hasDeepChanges) {
        [_dirCompares enumerateKeysAndObjectsUsingBlock:^(NSURL *dirURL, YPSDirectoryComparator *dirCmp, BOOL * stop) {
            [acc unionSet:dirCmp.allFilesInRightOnly];
        }];
    }
    return [acc copy];
}

- (NSSet<NSURL *> *) allFilesChanged {
    if (!_hasDeepChanges)
        return _filesChanged;

    NSMutableSet<NSURL *> *acc = [NSMutableSet setWithSet:_filesChanged];
    if (_hasDeepChanges) {
        [_dirCompares enumerateKeysAndObjectsUsingBlock:^(NSURL *dirURL, YPSDirectoryComparator *dirCmp, BOOL * stop) {
            [acc unionSet:dirCmp.allFilesChanged];
        }];
    }
    return [acc copy];
}

- (NSSet<NSURL *> *) dirsChanged {
    return [NSSet setWithArray:[_dirCompares allKeys]];
}

- (void) handleDirectoryChanges {
    if (!self.hasChanges) {
        DDLogInfo(@"No directory changes found");
        return; // nothing to be done
    }

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    // notify all left-side changes
    for (NSURL *fileURL in self.allFilesInLeftOnly) {
        if (_delegate)
            [_delegate handleLeftFileOnly:fileURL];

        NSString *notifyKey = _leftIsOlder ? YPSFileRemovedNotification : YPSFileAddedNotification;
        [nc postNotificationName:notifyKey
                          object:nil
                        userInfo:@{@"fileURL" : fileURL}];
    }

    // notify all right-side changes
    for (NSURL *fileURL in self.allFilesInRightOnly) {
        if (_delegate)
            [_delegate handleRightFileOnly:fileURL];

        NSString *notifyKey = _leftIsOlder ? YPSFileAddedNotification : YPSFileRemovedNotification;
        [nc postNotificationName:notifyKey
                          object:nil
                        userInfo:@{@"fileURL" : fileURL}];
    }

    // notify common changes
    for (NSURL *fileURL in self.allFilesChanged) {
        if (_delegate)
            [_delegate handleFileChanged:fileURL];

        [nc postNotificationName:YPSFileChangedNotification
                          object:nil
                        userInfo:@{@"fileURL" : fileURL}];
    }

    // fire final signal if we are at the root directory
    if (_delegate)
        [_delegate handleAllFilesHandled];
}
@end
