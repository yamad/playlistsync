//
//  YPSDirectorySnapshot.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/18/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "YPSDirectorySnapshot.h"


@implementation YPSDirectorySnapshot

- (instancetype) initWithURL:(NSURL *)dirPath {
    return [self initWithURL:dirPath shallowOnly:NO];
}

- (instancetype) initWithURL:(NSURL *) dirPath
                 shallowOnly:(BOOL) shallowOnly
{
    if ((self = [super init])) {
        _path = [dirPath copy];

        NSError *error = nil;
        _timestamp = [self makeSnapshotWithError:&error
                                     shallowOnly:shallowOnly];
    }
    return self;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"<%@: %p, \"%@, %@\">",
            [self class], self, _path, _timestamp];
}

- (NSDate *) makeSnapshotWithError:(NSError **)error {
    return [self makeSnapshotWithError:error shallowOnly:NO];
}

// save current state of directory
- (NSDate *) makeSnapshotWithError:(NSError **)error
                       shallowOnly:(BOOL) shallowCheck
{
    @autoreleasepool {
        NSFileManager *fm = [NSFileManager defaultManager];
        // attributes to snapshot
        NSArray *keys = @[NSURLFileResourceTypeKey,
                          NSURLGenerationIdentifierKey];

        // test if URL is a file/symlink
        NSPredicate *isFile = [NSPredicate predicateWithBlock:
                               ^BOOL(NSURL *item, NSDictionary *bindings) {
                                   NSString *resourceType = nil;
                                   BOOL populated = NO;
                                   populated = [item getResourceValue:&resourceType
                                                               forKey:NSURLFileResourceTypeKey
                                                                error:error];
                                   if (populated &&
                                       ([resourceType isEqual:NSURLFileResourceTypeRegular] ||
                                        [resourceType isEqual:NSURLFileResourceTypeSymbolicLink])) {
                                           return YES;
                                       }
                                   return NO;
                               }];
        // test if URL is a directory
        NSPredicate *isDir = [NSPredicate predicateWithBlock:
                              ^BOOL(NSURL *item, NSDictionary *bindings) {
                                  NSString *resourceType = nil;
                                  BOOL populated = [item getResourceValue:&resourceType
                                                                   forKey:NSURLFileResourceTypeKey
                                                                    error:error];
                                  if (populated &&
                                      [resourceType isEqual:NSURLFileResourceTypeDirectory]) {
                                      return YES;
                                  }
                                  return NO;
                              }];

        // get all immediate items
        // deeper items are captured by snapshots on descendants
        NSArray<NSURL *> *allItems = [fm contentsOfDirectoryAtURL:_path
                                       includingPropertiesForKeys:keys
                                                          options:0
                                                            error:error];
        NSDate *timestamp = [NSDate date]; // record snapshot time

        NSMutableDictionary *genIDs = @{}.mutableCopy;
        if (allItems) {
            [allItems enumerateObjectsUsingBlock:
             ^(NSURL *itemURL, NSUInteger idx, BOOL *stop) {
                 id gid;
                 [itemURL getResourceValue:&gid forKey:NSURLGenerationIdentifierKey error:nil];
                 genIDs[itemURL] = gid;
             }];
            _generationIDs = genIDs.copy;

            // save set of files
            _files   = [NSSet setWithArray:[allItems filteredArrayUsingPredicate:isFile]];

            // recurse subdirectories
            _subdirs = [NSMutableDictionary dictionary];

            if (!shallowCheck) {
                NSArray *dirValues = [allItems filteredArrayUsingPredicate:isDir];
                for (NSURL *url in dirValues) {
                    YPSDirectorySnapshot *ds = [[YPSDirectorySnapshot alloc] initWithURL:url];
                    _subdirs[url] = ds;

                    _timestamp = [_timestamp laterDate:ds.timestamp]; // save latest timestamp
                };
            }
        } else {
            return nil;
        }
        
        return timestamp;
    }
}

- (NSSet<NSURL *> *) allFiles {
    @autoreleasepool {
        NSMutableSet<NSURL *> *acc = [NSMutableSet setWithSet:self.files];
        [_subdirs enumerateKeysAndObjectsUsingBlock:^(NSURL *dirURL, YPSDirectorySnapshot *dirSnap, BOOL *stop) {
            [acc unionSet:dirSnap.allFiles];
        }];
        return [acc copy];
    }
}


@end
