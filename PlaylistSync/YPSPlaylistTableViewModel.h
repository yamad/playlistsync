//
//  YPSPlaylistTableViewModel.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/10/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YPSPlaylistNameProvider;
@class YPSPlaylistViewModel;

@interface YPSPlaylistTableViewModel : NSObject
@property (readonly) NSArray<YPSPlaylistViewModel *> *playlists;
@property (nonatomic, weak) YPSPlaylistNameProvider *nameProvider;

- (NSInteger) addNewPlaylist;  // add possible new playlist and return index
- (BOOL) removePlaylistAtIndex:(NSInteger)index;
- (void) syncToModel;
- (BOOL) validateTag:(NSString *)newTag;
@end