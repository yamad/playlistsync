//
//  AppDelegate.h
//  Shangri-La Playlists
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class YPSPlaylistSync;
@class YPSPlaylistNameProvider;

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property YPSPlaylistSync *playlistSync;
@property YPSPlaylistNameProvider *nameProvider;

- (void) setupFolderSync:(NSURL *) rootURL;
- (void) startFolderSync;
- (void) stopFolderSync;
- (void) startTimedStartFolderSync;
- (void) stopTimedStartFolderSync;
@end