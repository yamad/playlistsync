--
--  iTunesEventLink.applescript
--  PlaylistSync
--
--  provide iTunes applescript events to Obj-C
--
--  Created by Jason Yamada-Hanff on 5/15/16.
--  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
--
use AppleScript version "2.3.1"
use scripting additions
use framework "Foundation"


(* Result handling in Applescript/ObjC
    Objective-C doesn't understand native applescript errors, so we need a way to
	return errors from Applescript methods called by Objective-C.
	
   Every ObjC-exposed method returns a dictionary (NSDictionary) constructed by one of:
	 emitASErrorResult... --> {status: ITunesStatus_ASError, code:<>, message:<>}
       emitErrorResult...      --> {status: ITunesStatus_Error, code:<>, message:<>}
	 emitSuccessResult...  --> {status: ITunesStatus_OK, value: <>}

  where `emitASError...` returns an applescript error code and message
        `emitError...`   returns a custom error, with an ITunesResultCode in `code`
		`emitSuccess...` returns the succesful value in `value`
	
	To use, check the `status` entry. It holds an ITunesStatus value indicating success, error, or applescript error.
		The value of the return is held in the `value` entry of successful
		Error information (`code` and `message`) is available in error results

  Note: some of these semantics are inspired by Erlang message conventions
	*)

-- possible values of `status` field in a result. have to align these manually
property ITunesStatus_ASError : -2 -- applescript error, code is AS error number
property ITunesStatus_Error : -1 -- error, code is an ITunesStatus
property ITunesStatus_OK : 0

-- possible values of `code` field in an ITunesStatus_Error
property ITunesResultCode_Unspecified : 0
property ITunesResultCode_NotOpen : 1
property ITunesResultCode_PlaylistExists : 2
property ITunesResultCode_PlaylistCreated : 3
property ITunesResultCode_FileDoesNotExist : 4
property ITunesResultCode_PlaylistDoesNotExist : 5
property ITunesResultCode_TrackIDDoesNotExist : 6
property ITunesResultCode_TrackExists : 7
property ITunesResultCode_TrackAdded : 8
property ITunesResultCode_TrackIsPlaylistDuplicate : 9
property ITunesResultCode_FileAdded : 10
property ITunesResultCode_FileRejected : 11
property ITunesResultCode_TrackRemoved : 12
property ITunesResultCode_TrackDoesNotMatch : 13
property ITunesResultCode_NoTracks : 14

script ITunesEventLink
	use application "iTunes"
	property parent : class "NSObject"
	
	-- return a result indicating an applescript error to Objective-C
	to emitASErrorResultWithCode:code andMessage:msg
		return {status:ITunesStatus_ASError, code:code, message:msg}
	end emitASErrorResultWithCode:andMessage:
	
	-- return a result indicating an error to Objective-C
	to emitErrorResultWithCode:resultCode andMessage:msg
		return {status:ITunesStatus_Error, code:resultCode, message:msg}
	end emitErrorResultWithCode:andMessage:
	
	-- return a result indicating success to Objective-C
	to emitSuccessResultWithValue:value
        return {status:ITunesStatus_OK, code:ITunesResultCode_Unspecified, value:value}
	end emitSuccessResultWithValue:
	
	-- return a result indicating success to Objective-C
	to emitSuccessResultWithCode:resultCode
		return {status:ITunesStatus_OK, code:resultCode}
	end emitSuccessResultWithCode:
	
	-- return a result indicating success to Objective-C
	to emitSuccessResultWithCode:resultCode andValue:value
		return {status:ITunesStatus_OK, code:resultCode, value:value}
	end emitSuccessResultWithCode:andValue:
	
	-- -------------------------------
	-- Public API
	-- -------------------------------
	-- These functions are exposed to Objective-C and all return ITunesResult dictionaries
	
	-- rename a playlist by name
	to renamePlaylistFrom:oldName |to|:newName
		try
			-- find playlist with original name
			set matches to (playlists whose name is oldName)
			if (matches is {}) then
				return (my emitErrorResultWithCode:ITunesResultCode_PlaylistDoesNotExist ¬
					andMessage:oldName)
			end if
			
			-- set new name
			set pl to item 1 of matches
			set name of pl to newName
			
			-- return playlist id on success
			return (my emitSuccessResultWithValue:(id of pl))
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end renamePlaylistFrom:|to|:
	
	
	-- add existing track with database ID to a playlist
	to addTrackID:trackID toPlaylist:plName
		try
			set trackIDAS to trackID as integer
			set plNameAS to plName as text
			
			-- make sure track exists
			try
				set |track| to (some track of my getMusicLibrary() whose database ID is trackIDAS)
			on error number -1728 -- could not get track
				return (my emitErrorResultWithCode:ITunesResultCode_TrackIDDoesNotExist andMessage:trackIDAS)
			end try
			
			-- make sure playlist exists. cannot fail (except by applescript error)
			-- do this after checking track exists, if no track we don't need the playlist
			my privateEnsurePlaylistExists:plNameAS
			set plRef to playlist plNameAS
			
			-- add track using direct references. returns a success result already
			return (my privateAddTrackID:trackIDAS toPlaylistRef:plRef)
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end addTrackID:toPlaylist:
	
	-- add file to playlist name. doesn't check for duplicates (do this in ObjC)
	to addFileWithoutChecks:trackPath toPlaylist:plName
		try
			-- coerce to iTunes-friendly path. yes, apparently it's this complicated
			try
				set trackPathAS to (((trackPath as text) as POSIX file) as alias)
			on error number -1700 -- can't access file
				return (my emitErrorResultWithCode:ITunesResultCode_FileDoesNotExist andMessage:trackPath)
			end try
			
			-- note: checking for duplicate tracks within applescript causes an overflow
			-- no easy way to query by location. have to handle such checks externally
			
			-- create new track in iTunes
			try
				set t to (add trackPathAS to (my getMusicLibrary()))
				t -- try to evaluate t, will cause error if `add` rejected the file
			on error
				return (my emitErrorResultWithCode:ITunesResultCode_FileRejected andMessage:trackPath)
			end try
			
			-- add created track to playlist
			set res to (my addTrackID:(database ID of t) toPlaylist:plName) -- returns ITunesResult
			return (my emitSuccessResultWithCode:ITunesResultCode_FileAdded andValue:(value of res))
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end addFileWithoutChecks:toPlaylist:
	
	
	-- add a set of files (by path) to the playlist with given name.
	to addFileList:filePaths toPlaylist:plName
		try
			set filePathsAS to filePaths as list
			set res to {}
			
			repeat with filePath in filePathsAS
				set res to res & (my addFileWithoutChecks:filePath toPlaylist:plName)
			end repeat
			return (my emitSuccessWithValue:res)
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end addFileList:toPlaylist:
	
	
	-- remove track by database ID. uses file path to make sure we've got the right track
	to removeTrackByID:trackID andFile:trackPath
		try
			set trackIDAS to trackID as integer
			
			-- coerce to iTunes-friendly path. yes, apparently it's this complicated
			try
				set trackPathAS to (((trackPath as text) as POSIX file) as alias)
			on error number -1700 -- can't access file
				return (my emitErrorResultWithCode:ITunesResultCode_FileDoesNotExist andMessage:trackPath)
			end try
			
			-- make sure track exists
			try
				set |track| to (some track of my getMusicLibrary() whose database ID is trackIDAS)
			on error number -1728 -- could not get track
				return (my emitErrorResultWithCode:ITunesResultCode_TrackIDDoesNotExist andMessage:trackIDAS)
			end try
			
			if (location of |track| is equal to trackPathAS) then
				delete |track|
				return (my emitSuccessResultWithCode:ITunesResultCode_TrackRemoved andValue:trackIDAS)
			else
				return (my emitErrorResultWithCode:ITunesResultCode_TrackDoesNotMatch andMessage:trackIDAS)
			end if
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end removeTrackByID:andFile:
	
	
	-- return all tracks in library
	to getAllFileTrackInfo()
		try
			if (file tracks of (get some playlist whose special kind is Music) is {}) then
				return (my emitErrorResultWithCode:ITunesResultCode_NoTracks andMessage:"Library has no tracks")
			end if
			
			set res to {|dbID|:database ID, |location|:location} ¬
				in (every file track in (get some playlist whose special kind is Music))
			return (my emitSuccessResultWithValue:res)
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end getAllFileTrackInfo
	
	-- return all tracks from a playlist
	-- gives database id (a unique identifier) and location in separate lists
	to getAllTracksFromPlaylist:plName
		try
			set plNameAS to plName as text
			if not (exists playlist plNameAS) then
				return (my emitErrorResultWithCode:ITunesResultCode_PlaylistDoesNotExist andMessage:plName)
			end if
			
			if (file tracks of playlist plNameAS is {}) then
				return (my emitErrorResultWithCode:ITunesResultCode_NoTracks andMessage:"Playlist has no tracks")
			end if
			
			set res to {|dbID|:database ID, |location|:location} ¬
				in (every file track in playlist plNameAS)
			return (my emitSuccessResultWithValue:res)
		on error msg number code
			return (my emitASErrorResultWithCode:code andMessage:msg)
		end try
	end getAllTracksFromPlaylist:
	
	
	-- -----------------------------
	-- Internal applescript handlers
	-- -----------------------------
	-- These handlers are used internally, and do not necessarily return ITunesResults
	
	-- add existing track with database ID to a playlist reference. assumes playlist exists
	to privateAddTrackID:trackID toPlaylistRef:plRef
		set matchedTracks to (tracks of plRef whose database ID is trackID) -- check if already in playlist
		if (matchedTracks is {}) then
			set |track| to (some track of ¬
				(get some playlist whose special kind is Music) ¬
					whose database ID is trackID)
			duplicate |track| to plRef -- `duplicate` puts existing library track on playlist
			set code to ITunesResultCode_TrackAdded
		else
			set |track| to (item 1 of matchedTracks)
			set code to ITunesResultCode_TrackIsPlaylistDuplicate
		end if
		return (my emitSuccessResultWithCode:code ¬
			andValue:({|dbID|:database ID, |location|:location} of |track|))
		-- can't access any information about the playlist, for some reason
	end privateAddTrackID:toPlaylistRef:
	
	
	-- ensure playlist with `name` exists, check and create.
	-- returns either 'PlaylistCreated' or 'PlaylistExists' result codes
	to privateEnsurePlaylistExists:plName
		set lName to plName as text
		if not (exists playlist lName) then
			make playlist with properties {name:lName}
			return ITunesResultCode_PlaylistCreated
		else
			return ITunesResultCode_PlaylistExists
		end if
	end privateEnsurePlaylistExists:
	
	
	-- get reference to iTunes music library
	to getMusicLibrary()
		return (get some playlist whose special kind is Music)
	end getMusicLibrary
end script