//
//  YPSDirectoryComparator.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/25/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

@import Foundation;

@class YPSDirectorySnapshot;

@protocol YPSDirectoryComparatorChangeDelegate
- (void) handleLeftFileOnly:(NSURL *)fileURL;
- (void) handleRightFileOnly:(NSURL *)fileURL;
- (void) handleFileChanged:(NSURL *)fileURL;
- (void) handleAllFilesHandled;
@end

@interface YPSDirectoryComparator : NSObject
@property (readonly) NSMutableSet<NSURL *> *filesInLeftOnly;
@property (readonly) NSMutableSet<NSURL *> *filesInRightOnly;
@property (readonly) NSMutableSet<NSURL *> *filesInCommon;
@property (readonly) NSMutableSet<NSURL *> *filesChanged;
@property (readonly) NSMutableSet<NSURL *> *dirsInLeftOnly;
@property (readonly) NSMutableSet<NSURL *> *dirsInRightOnly;
@property (readonly) NSMutableSet<NSURL *> *dirsInCommon;
@property (readonly) NSSet<NSURL *> *dirsChanged;

/** flat lists of all distinct files in subdirectories */
@property (readonly) NSMutableSet<NSURL *> *filesInLeftOnlyDirs;
@property (readonly) NSMutableSet<NSURL *> *filesInRightOnlyDirs;

@property (readonly) NSMutableDictionary<NSURL *, YPSDirectoryComparator *> *dirCompares;
@property (readonly) NSDate *timestamp;
@property (readonly) BOOL hasShallowChanges;
@property (readonly) BOOL hasDeepChanges;
@property (readonly) BOOL hasChanges;

/** convenience accessors to all changes, shallow and deep */
@property (readonly) NSSet<NSURL *> *allFilesInLeftOnly;
@property (readonly) NSSet<NSURL *> *allFilesInRightOnly;
@property (readonly) NSSet<NSURL *> *allFilesChanged;

- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                        checkDirs:(NSSet<NSURL *> *) dirsToCheck
                         delegate:(id<YPSDirectoryComparatorChangeDelegate>) delegate
                      shallowOnly:(BOOL) shallowCheck
                           isRoot:(BOOL) isRootDir NS_DESIGNATED_INITIALIZER;
- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                        checkDirs:(NSSet<NSURL *> *) dirsToCheck
                         delegate:(id<YPSDirectoryComparatorChangeDelegate>) delegate;
- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap
                      shallowOnly:(BOOL) shallowCheck;
- (instancetype) initWithSnapshot:(YPSDirectorySnapshot *) leftSnap
                         andOther:(YPSDirectorySnapshot *) rightSnap;
- (instancetype) init NS_UNAVAILABLE;

/** post notifications of all changes */
- (void) handleDirectoryChanges;
@end


/** Notifications posted by a comparator via `notify` */
extern NSString *const YPSFileAddedNotification;
extern NSString *const YPSFileRemovedNotification;
extern NSString *const YPSFileChangedNotification;
