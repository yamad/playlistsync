//
//  constants.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 5/29/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "constants.h"

NSString *const kYPSDefaultPlaylistTag       = @"DEFAULT_PLAYLIST";

// matches an uppercase letter and following letter or number
// occurring after a hash symbol '#' as `tag`.
// will not match if capital letter or number comes afterwards also
NSString *const kYPSTagRegex                 = @"#(?<tag>[A-Z][A-Z0-9])(?![A-Z0-9])";

NSString *const kYPSRootFolderURLKey         = @"RootFolderURLKey";
NSString *const kYPSPlaylistNameExcelFileKey = @"PlaylistNameExcelFileKey";
NSString *const kYPSDefaultPlaylistNameKey   = @"DefaultPlaylistNameKey";
NSString *const kYPSShouldAutoRestartKey     = @"ShouldAutoRestartKey";
NSString *const kYPSTrackDeletionsKey        = @"TrackDeletionsKey";
NSString *const kYPSTrackChangesKey          = @"TrackChangesKey";

NSString *const kYPSStartCountdownTickNotification        = @"kYPSStartCountdownTickNotification";
NSString *const kYPSPlaylistSyncStartNotification         = @"kYPSPlaylistSyncStartNotification";
NSString *const kYPSPlaylistSyncStopNotification          = @"kYPSPlaylistSyncStopNotification";
NSString *const kYPSStartProgressOperationNotification    = @"kYPSStartProgressOperationNotification";
NSString *const kYPSStopProgressOperationNotification     = @"kYPSStopProgressOperationNotification";
NSString *const kYPSApplicationFinishedLaunchNotification = @"kYPSApplicationFinishedLaunchNotification";
NSString *const kYPSNewTagDiscoveredNotification          = @"kYPSNewTagDiscoveredNotification";