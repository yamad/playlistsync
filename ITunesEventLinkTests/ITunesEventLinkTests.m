//
//  ITunesEventLinkTests.m
//  ITunesEventLinkTests
//
//  Created by Jason Yamada-Hanff on 6/6/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <AppleScriptObjC/AppleScriptObjC.h>

#import "ITunesEventLink.h"

@interface ITunesEventLinkTests : XCTestCase
@property id iTLink;
@end

@implementation ITunesEventLinkTests

- (void)setUp {
    [super setUp];
    // test code does not run in the mainBundle!!
    [[NSBundle bundleForClass:[self class]] loadAppleScriptObjectiveCScripts];
    _iTLink = [NSClassFromString(@"ITunesEventLink") new];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testThatItGetsAllFileTrackInformation {
    ITunesResult *itr = [_iTLink getAllFileTrackInfo];

    XCTAssertNotNil(itr);

    ITunesStatus status = [itr[@"status"] integerValue];
    XCTAssertEqual(ITunesStatus_OK, status);

    NSDictionary *d = itr[@"value"];
    NSArray *ids = d[@"dbID"];
    NSArray *locs = d[@"location"];

    XCTAssertNotNil(ids);
    XCTAssertNotNil(locs);
}

@end
