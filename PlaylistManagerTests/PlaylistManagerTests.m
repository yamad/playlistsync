//
//  PlaylistManagerTests.m
//  PlaylistManagerTests
//
//  Created by Jason Yamada-Hanff on 6/6/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <AppleScriptObjC/AppleScriptObjC.h>

#import "JYHRandomAudioFolderGenerator.h"
#import "JYHAudioFileGenerator.h"
#import "YPSPlaylistSync.h"

@interface PlaylistManagerTests : XCTestCase
@property NSURL *testDirURL;
@end

@implementation PlaylistManagerTests

- (void)setUp {
    [super setUp];
    _testDirURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"psTest"] isDirectory:YES];
}

- (void)tearDown {
    [super tearDown];
    [[NSFileManager defaultManager] removeItemAtURL:_testDirURL error:nil];
}

- (void) testThatItAddsAFile {
    JYHAudioFileGenerator *g = [[JYHAudioFileGenerator alloc] initWithFileURL:[NSURL fileURLWithPath:@"a.m4a"] duration:2];
    [g generateFile];  // async make file

    YPSPlaylistSync *s = [[YPSPlaylistSync alloc] initWithRootURL:_testDirURL startImmediately:YES];

    sleep(10);
    XCTAssertEqual(1, [s.trackStore count]);
}

@end
