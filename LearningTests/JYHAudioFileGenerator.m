//
//  JYHAudioFileGenerator.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "JYHAudioFileGenerator.h"

#import <EZAudioOSX/EZMicrophone.h>
#import <EZAudioOSX/EZRecorder.h>

@interface JYHAudioFileGenerator () <EZMicrophoneDelegate, EZRecorderDelegate>
@property EZMicrophone *mic;
@property EZRecorder   *rec;
@end


@implementation JYHAudioFileGenerator

- (instancetype) initWithFileURL:(NSURL *)outputURL
                          duration:(NSInteger)duration
{
    self = [super init];
    if (self) {
        _outputURL = [outputURL copy];
        _fileDuration = duration;

        EZRecorderFileType ftype;
        if ([[_outputURL pathExtension] isEqual:@"aif"])
            ftype = EZRecorderFileTypeAIFF;
        else if ([[_outputURL pathExtension] isEqual:@"m4a"])
            ftype = EZRecorderFileTypeM4A;
        else
            ftype = EZRecorderFileTypeWAV;

        _mic = [EZMicrophone microphoneWithDelegate:self];
        if (!_mic) return nil;

        _rec = [EZRecorder recorderWithURL:_outputURL
                              clientFormat:[_mic audioStreamBasicDescription]
                                  fileType:ftype
                                  delegate:self];
        if (!_rec) return nil;
    }

    return self;
}

- (instancetype) initWithFilePath:(NSString *)outputPath duration:(NSInteger)duration
{
    NSURL *url = [NSURL fileURLWithPath:[outputPath stringByStandardizingPath]
                            isDirectory:NO];
    return [self initWithFileURL:url duration:duration];
}

- (void) generateFile {
    [self generateFileWithCompletionBlock:^(void){}];
}

- (void) generateFileWithCompletionBlock:(void (^)(void))block {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, _fileDuration * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_global_queue(0, 0), ^(void) {
        [self finishFile];
        block();
    });
    [_mic startFetchingAudio];
}

- (void) finishFile {
    [_mic stopFetchingAudio];
    [_rec closeAudioFile];
}


// delegate methods taken from EZAudio example RecordFile project

#pragma mark - EZMicrophoneDelegate
- (void)   microphone:(EZMicrophone *)microphone
        hasBufferList:(AudioBufferList *)bufferList
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels
{
    [_rec appendDataFromBufferList:bufferList
                    withBufferSize:bufferSize];
}

#pragma mark - EZRecorderDelegate
- (void)recorderDidClose:(EZRecorder *)recorder
{
    recorder.delegate = nil;
}

@end
