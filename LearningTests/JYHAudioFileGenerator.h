//
//  JYHAudioFileGenerator.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYHAudioFileGenerator : NSObject
@property (nonatomic, copy) NSURL *outputURL;
@property (nonatomic) NSInteger fileDuration;

- (instancetype) initWithFilePath:(NSString *)outputPath
                        duration:(NSInteger)length;
- (instancetype) initWithFileURL:(NSURL *)outputURL
                          duration:(NSInteger)length;

- (void) generateFile;
- (void) generateFileWithCompletionBlock:(void (^)(void))block;
@end
