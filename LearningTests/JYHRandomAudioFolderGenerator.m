//
//  JYHRandomAudioFolderGenerator.m
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import "JYHRandomAudioFolderGenerator.h"

#import "JYHAudioFileGenerator.h"
#import "JYHAudioFileGenerator.h"

@implementation JYHRandomAudioFolderGenerator

- (instancetype) initWithRootURL:(NSURL *) rootURL count:(NSUInteger) fileCount maxDuration:(NSUInteger)maxDur {
    self = [super init];
    if (!self) return nil;

    // make sure folder exists
    [[NSFileManager defaultManager] createDirectoryAtURL:rootURL withIntermediateDirectories:YES attributes:nil error:nil];

    // make audio files with random names
    dispatch_group_t group = dispatch_group_create();
    for (NSUInteger i = 0; i < fileCount; i++) {
        NSString *filePath = [NSString stringWithFormat:@"%@.m4a", [self generateRandomString]];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath isDirectory:NO relativeToURL:rootURL];
        NSUInteger dur = arc4random_uniform((u_int32_t)maxDur);
        JYHAudioFileGenerator *g = [[JYHAudioFileGenerator alloc] initWithFileURL:fileURL duration:dur];

        // use groups to make thread wait until all files are generated
        dispatch_group_enter(group);
        [g generateFileWithCompletionBlock:^() {
            dispatch_group_leave(group);
        }];
    }

    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    return self;
}

- (instancetype) initWithRootURL:(NSURL *)rootURL count:(NSUInteger)fileCount {
    return [self initWithRootURL:rootURL count:fileCount maxDuration:1];
}


+ (void) generateFromList:(NSArray<NSString *> *) fileList relativeToURL:(NSURL *) root {
    NSFileManager *fm = [NSFileManager defaultManager];

    // make sure root folder exists
    //[fm createDirectoryAtURL:root withIntermediateDirectories:YES attributes:nil error:nil];

    // files specified by path. make each one.
    [fileList
     enumerateObjectsWithOptions:NSEnumerationConcurrent
     usingBlock:^(NSString *path, NSUInteger idx, BOOL *stop) {
         NSURL *fileURL = [NSURL fileURLWithPath:path relativeToURL:root];

         // make sure parent directory exists
         [fm createDirectoryAtURL:[fileURL URLByDeletingLastPathComponent]
      withIntermediateDirectories:YES attributes:nil error:nil];

         // make file with arbitrary contents
         [fm createFileAtPath:[fileURL path]
                     contents:[NSData dataWithBytes:(char[]){0x01} length:1]
                   attributes:nil];
     }];
}

- (NSString *) generateRandomString {
    return [self generateRandomStringWithLength:arc4random_uniform(50)];
}

- (NSString *) generateRandomStringWithLength:(NSUInteger) length {
    NSString *alphabet  = @" -#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:length];
    for (NSUInteger i = 0U; i < 20; i++) {
        u_int32_t r = arc4random_uniform((u_int32_t)[alphabet length]);
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return [s copy];
}

- (NSString *) generateRandomTag {
    NSString *alphanum  = @"ABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSString *alpha     = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    unichar c1 = [alpha characterAtIndex:arc4random_uniform((u_int32_t)[alpha length])];
    unichar c2 = [alphanum characterAtIndex:arc4random_uniform((u_int32_t)[alphanum length])];
    return [NSString stringWithFormat:@"#%C%C", c1, c2];
}

@end
