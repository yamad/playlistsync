//
//  LearningTests.m
//  LearningTests
//
//  Created by Jason Yamada-Hanff on 6/1/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "JYHAudioFileGenerator.h"
#import "JYHRandomAudioFolderGenerator.h"

@interface LearningTests : XCTestCase

@end

@implementation LearningTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


/** Creating an NSURL from a relative or absolute path */
- (void) testRelativePathHandling {
    // relative/absolute paths
    NSString *relWDot     = @"./file.txt";
    NSString *relNoPrefix = @"file.txt";
    NSString *absWTilde   = @"~/file.txt";
    NSString *absFull     = @"/Users/jyh/file.txt";

    // parent path. note that `dir` must have isDirectory:YES and/or trailing slash
    NSURL *dir = [NSURL fileURLWithPath:@"/Users/jyh/" isDirectory:YES];

    // turn everything into a full absolute URL
    NSURL *urlWDot     = [NSURL fileURLWithPath:[relWDot stringByStandardizingPath]     relativeToURL:dir];
    NSURL *urlNoPrefix = [NSURL fileURLWithPath:[relNoPrefix stringByStandardizingPath] isDirectory:NO relativeToURL:dir];
    NSURL *urlWTilde   = [NSURL fileURLWithPath:[absWTilde stringByStandardizingPath]  isDirectory:NO relativeToURL:dir];
    NSURL *urlFull     = [NSURL fileURLWithPath:[absFull stringByStandardizingPath]  isDirectory:NO relativeToURL:dir];

    NSURL *expected    = [NSURL fileURLWithPath:@"/Users/jyh/file.txt"];

    // direct equality test _does not_ work
    XCTAssertEqualObjects([expected path], [urlWDot path]);
    XCTAssertEqualObjects([expected path], [urlNoPrefix path]);
    XCTAssertEqualObjects([expected path], [urlWTilde path]);
    XCTAssertEqualObjects([expected path], [urlFull path]);
}

- (void) testCreateAudioFile {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Finish audio file"];
    JYHAudioFileGenerator *gen = [[JYHAudioFileGenerator alloc] initWithFilePath:@"~/test.m4a" duration:10];
    [gen generateFileWithCompletionBlock:^(void) {
        [expectation fulfill]; // needed to force test to wait until file is completed
    }];
    [self waitForExpectationsWithTimeout:12.0 handler:nil];

    NSURL *tmpURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), @"psTest"] isDirectory:YES];
    JYHRandomAudioFolderGenerator *rafg = [[JYHRandomAudioFolderGenerator alloc] initWithRootURL:tmpURL count:20];
    [[NSFileManager defaultManager] removeItemAtURL:tmpURL error:nil];
}

- (void) testFileContentsComparison {
    // document failing behavior of -contentsEqualAtPath:andPath:

    // generate a file
    XCTestExpectation *e = [self expectationWithDescription:@"audio file done"];
    JYHAudioFileGenerator *g = [[JYHAudioFileGenerator alloc] initWithFilePath:@"~/test.m4a" duration:2];
    [g generateFileWithCompletionBlock:^(void) {
        [e fulfill];
    }];
    [self waitForExpectationsWithTimeout:12.0 handler:nil];

    // make an exact copy
    [[NSFileManager defaultManager] copyItemAtPath:@"~/test.m4a" toPath:@"~/test2.m4a" error:nil];

    // compare exact copies using -contentsEqualAtPath:andPath:
    BOOL isEqual = [[NSFileManager defaultManager] contentsEqualAtPath:@"~/test.m4a" andPath:@"~/test2.m4a"];

    // this does not work!
    // isEqual should be true, but is actually false
    XCTAssertFalse(isEqual);

    // compare actually identical files
    isEqual = [[NSFileManager defaultManager] contentsEqualAtPath:@"~/test.m4a" andPath:@"~/test.m4a"];
    // this doesn't work either!
    // isEqual should be true. this is literally the _same file_
    XCTAssertFalse(isEqual);
}

@end
