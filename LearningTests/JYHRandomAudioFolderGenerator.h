//
//  JYHRandomAudioFolderGenerator.h
//  PlaylistSync
//
//  Created by Jason Yamada-Hanff on 6/3/16.
//  Copyright © 2016 Jason Yamada-Hanff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYHRandomAudioFolderGenerator : NSObject
- (instancetype) initWithRootURL:(NSURL *) rootURL count:(NSUInteger) fileCount;
- (instancetype) initWithRootURL:(NSURL *) rootURL count:(NSUInteger) fileCount maxDuration:(NSUInteger) maxDur;

+ (void) generateFromList:(NSArray<NSString *> *)fileList relativeToURL:(NSURL *)root;
@end
